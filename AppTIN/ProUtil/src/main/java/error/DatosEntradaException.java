package error;

public class DatosEntradaException extends Exception {
    private String errorType;
    private String errorCode;
	/**
     *
     */
    @SuppressWarnings("compatibility:-6487522167612428427")
    private static final long serialVersionUID = 1L;

	public DatosEntradaException(String msg) {
		super(msg);

	}

	public DatosEntradaException(String msg, Throwable e) {
		super(msg, e);

	}
    public DatosEntradaException (String msg, String errorType, String errorCode) {
            super(msg);
            this.errorType = errorType;
            this.errorCode = errorCode;
    }
    
    
    public String getErrorType() {
            return errorType;
    }

    /**
     * @return the errorCode
     */
    public String getErrorCode() {
            return errorCode;
    }
}
