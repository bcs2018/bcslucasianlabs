package error;

import java.sql.SQLException;

import org.pmw.tinylog.Logger;


public class ExceptionStarGeneric extends Exception {

    @SuppressWarnings("compatibility:-2618108564929779752")
    private static final long serialVersionUID = 1889258277124816083L;
    private transient CrearError crearError = CrearError.getInstace();
    private Error error;

    public ExceptionStarGeneric(Error error, Throwable cause) {
        super(error.getDescription(), cause);
        this.error = error;
    }
    
    public ExceptionStarGeneric(String code, String descripcionComplementaria) {
        this.error = crearError.getError(code, descripcionComplementaria);

        Logger.error("Error : Desc:" + error.getDescription() + ". Code: " + error.getCode() + ". Type: " +
                     error.getType());
        Logger.error("Stack :", this);
    }

    public ExceptionStarGeneric(String code, String descripcionComplementaria, Throwable cause) {
        super(descripcionComplementaria, cause);
        this.error = crearError.getError(code, descripcionComplementaria);

        Logger.error("Error : Desc:" + error.getDescription() + ". Code: " + error.getCode() + ". Type: " +
                     error.getType());
        Logger.error("Stack :", this);
    }

    public ExceptionStarGeneric(String codigo, String mensaje, Exception e) {
        super(mensaje, e);

        if (mensaje == null)
            mensaje = "";

        if (e instanceof SQLException) {
            mensaje =
                    mensaje + " " + (e).getMessage() + " " + ((SQLException)e).getSQLState() + " " + ((SQLException)e).getErrorCode();

            if (((SQLException)e).getErrorCode() != 0) {
                codigo = "" + ((SQLException)e).getErrorCode();
                for (int i = codigo.length(); i < 5; i++) {
                    codigo = "0" + codigo;
                }
            }
        } else if (e != null) {
            mensaje = mensaje + " " + e.getMessage();
        }
    }

    public ExceptionStarGeneric(String description, String type, String code) {
        try {
            this.error =
                    new Error(description, type, code);

            Logger.error("Error : Desc:" + error.getDescription() + ". Code: " + error.getCode() + ". Type: " +
                         error.getType());
            Logger.error("Stack :", this);
        } catch (Exception e) {
            CrearError ce = CrearError.getInstace();
            this.error = ce.getError("80008", e.getMessage());
            Logger.error(e,"Error creando la respuesta de error");
            Logger.error("Error : Desc:" + error.getDescription() + ". Code: " + error.getCode() + ". Type: " +
                         error.getType());
        }
    }

    public static ExceptionStarGeneric isExceptionStarGeneric(String codigo, String mensaje, Exception e) {
        if (e instanceof ExceptionStarGeneric) {
            return (ExceptionStarGeneric)e;
        } else {
            ExceptionStarGeneric expStaGen = new ExceptionStarGeneric(codigo, mensaje, e);
            return expStaGen;
        }
    }

    public Error getError() {
        return error;
    }
}
