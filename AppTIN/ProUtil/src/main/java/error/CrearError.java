package error;

import java.io.InputStream;

import java.util.Properties;

import org.pmw.tinylog.Logger;

import util.PropertiesLoader;


public class CrearError {
//    static private PropertiesLoader properties = null;
    static private CrearError singleton = null;

    private CrearError() {
        super();
      //  getSystemFileProperty()
    }

   /* private void getSystemFileProperty() {
        Logger.info("Cargando archivo de codigos de error en CrearError");
        try {
            Logger.info("Alistando cargue CodigosErrores.properties");
            InputStream is = this.getClass().getResourceAsStream("CodigosErrores.properties");
            properties = new Properties();
            Logger.info("Captura de class CodigosErrores.properties");
            properties.load(is);
            is.close();
        } catch (Exception e) {
            Logger.error(e,"Error cargando el archivo de propiedades en CrearError linea 37 ");
            properties = null;
        }
    }*/

    static synchronized public CrearError getInstace() {
        if (singleton == null) {
            singleton = new CrearError();
        }
        return singleton;
    }

    public Error getError(String code, String descripcionComplementaria) {
        Error error = new Error();
        String descripcionGeneral = null;
        if (PropertiesLoader.getProperty("START") != null) {
            System.out.println("Dentro de get error if en true");
            Logger.info("getError:code: "+code);
            Logger.info("getError:properties.getProperty(code): "+PropertiesLoader.getProperty(code));
            descripcionGeneral =PropertiesLoader.getProperty(code);
            Logger.info("getError:descripcionGeneral: "+descripcionGeneral);
            descripcionGeneral = descripcionGeneral + ". " + descripcionComplementaria;
        } else
            System.out.println("Dentro de get error if en false");
            descripcionGeneral = descripcionComplementaria;

        error.setCode(code);
        error.setDescription(descripcionGeneral);
        error.setType(Error.getType(code));
        return error;
    }
}
