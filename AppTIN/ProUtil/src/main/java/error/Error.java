package error;

import java.io.Serializable;

public class Error implements Serializable {
    private String description;
    private String type;
    private String code;
   
    static final public  String DBA = "DBA";
    static final public String COM = "COM";
    static final public String VAL = "VAL";
    static final public String OTR = "OTR";
    static final public String ERROR="ER";
    static final public String OK="OK";

    public Error() {
        super();
    }

    public Error(String description, String type, String code) {
        super();
        this.description = description;
        this.type = type;
        this.code = code;
      
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

  


    static public String getType(String code) {
        switch (code.charAt(0)) {
        case '9':
            return Error.DBA;

        case '8':
            return Error.VAL;

        case '7':
            return Error.COM;

        default:
            return Error.OTR;
        }

    }
}
