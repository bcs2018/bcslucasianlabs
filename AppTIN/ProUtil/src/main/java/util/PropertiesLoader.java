package util;

import java.util.ResourceBundle;

import org.pmw.tinylog.Logger;

public class PropertiesLoader {

    private static ResourceBundle recursos;

    /**
     * Metodo encargado de devolver una nueva instancia de la clase.
     * @return
     * @throws Exception
     */
    private static ResourceBundle getInstance() throws Exception {
       // Logger.info("INICIANDO:propsLoader:::::" + recursos);
        if (recursos == null) {
            recursos = ResourceBundle.getBundle("configuracion");
        }
        return recursos;
    }

    /**
     * Metodo que se encarga de retornar una determinada propiedad.
     * @param prop
     * @return
     */
    public static String getProperty(String prop) {
        try {
           // Logger.de("INICIANDO:getProperty:::::" + prop);
           // Logger.info("getProperty:::::" + getInstance().getString(prop));
            return getInstance().getString(prop);
        } catch (Exception e) {
            Logger.error(e, "Error cargando propiedad_getProperty" + prop + e.getMessage() + e);
            return null;
        }
    }
}