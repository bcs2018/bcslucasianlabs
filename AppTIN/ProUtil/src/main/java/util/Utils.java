package util;

import error.DatosEntradaException;

import java.math.BigDecimal;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;

import java.text.DateFormat;

import java.text.DecimalFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import java.util.Map;

import static util.ErrorConstants.ERINP3;
import static util.ErrorConstants.ERINP4;
import static util.ErrorConstants.ERINP5;
import static util.ErrorConstants.TYPE_ERROR_ERINP;

/**
 * La clase <code>Utils</code> provee m&eacute;todos utilitarios para todas la
 * aplicaci&oacute;n.
 *
 */
public class Utils {
    public static final String DATE_FORMAT_1 = "yyyyMMdd";
    public static final String DATE_FORMAT_2 = "HH:MM";
    public static final String DATE_FORMAT_3 = "dd/MM/yyyy";

    /**
     * @param fecha
     * @param formato
     * @throws DatosEntradaException
     */
    public static void validarFecha(String fecha, String formato) throws DatosEntradaException {
        if (fecha.trim().equals("")) {
            throw new DatosEntradaException(ERINP3, TYPE_ERROR_ERINP, "3");
        } else {
            string2Calendar(fecha, formato);
        }
    }

    public static String calendar2String(Calendar calendar, String pattern) {
        SimpleDateFormat formateDate = new SimpleDateFormat(pattern);

        return formateDate.format(calendar.getTime());
    }

    public static String formatDate(String fecha, String formatoInicio,
                                    String formatoFinal) throws DatosEntradaException {
        try {
            SimpleDateFormat formateDate = new SimpleDateFormat(formatoInicio);

            Date d = formateDate.parse(fecha);

            formateDate = new SimpleDateFormat(formatoFinal);

            return formateDate.format(d);
        } catch (ParseException e) {
            throw new DatosEntradaException(ERINP4, TYPE_ERROR_ERINP, "4");
        }
    }

    public static Calendar string2Calendar(String fecha, String formato) throws DatosEntradaException {
        Calendar c = null;

        try {
            SimpleDateFormat formateDate = new SimpleDateFormat(formato);

            Date d = formateDate.parse(fecha);

            c = Calendar.getInstance();
            c.setTime(formateDate.parse(formateDate.format(d)));
        } catch (ParseException e) {
            throw new DatosEntradaException(ERINP4, TYPE_ERROR_ERINP, "4");
        }
        return c;
    }
    
    /**
     *
     * @param cadena
     * @return
     * @throws DatosEntradaException
     */
    public static Integer string2Int(String cadena) throws DatosEntradaException {
        try {
            return Integer.parseInt(cadena);
        } catch (Exception e) {
            throw new DatosEntradaException(ERINP5, TYPE_ERROR_ERINP, "5");
        }
    }
    
    /**
     *
     * @param cadena
     * @return
     * @throws DatosEntradaException
     */
    public static Long string2Long(String cadena) throws DatosEntradaException {
        try {
            return Long.parseLong(cadena);
        } catch (Exception e) {
            throw new DatosEntradaException(ERINP5, TYPE_ERROR_ERINP, "5");
        }
    }
    
    /**
     *
     * @param datos
     * @throws DatosEntradaException
     */
    public static void validarDatos(String[] datos) throws DatosEntradaException {
        for (String dato : datos) {
            if (dato.equals("")) {
                throw new DatosEntradaException(ERINP3, TYPE_ERROR_ERINP, "3");
            }
        }
    }

    /**
     * Funci&oacute;n para alinear un campo tipo String a la izquierda
     *
     * @param s Campo a formatear
     * @param l Longitud total del campo
     * @return String Campo formateado
     */
    public static String pad(String s, int l) {
        String blancos = "             ";

        for (int i = 1; i < 1000; i++)
            blancos = blancos + "  ";

        if (s == null)
            return blancos.substring(0, l);
        else
            return (s + blancos).substring(0, l);
    }

    /**
     * Funci&oacute;n para alinear un campo tipo Integer a la derecha, y
     * completarlo con ceros
     *
     * @param n Campo a formatear
     * @param l Longitud del campo
     * @return String Campo formateado
     */
    public static String pad(Integer n, int l) {
        String ceros = "0000000000000000";

        return (ceros + n).substring((16 + n.toString().length()) - l);
    }
    
    /**
     *
     * @param format
     * @return
     */
    public static String obtenerFecha(String format) {
        Date date = new Date();
        DateFormat hourdateFormat = new SimpleDateFormat(format);
        return hourdateFormat.format(date);
    }

    /**
     *
     * @param texto
     * @param tamano
     * @return salida
     */
    public static String completarTexto(String texto, Integer tamano) {
        String salida = "";
        if (texto == null) {
            texto = "";
        }
        char[] textoChar = texto.toCharArray();
        for (int i = 0; i < tamano; i++) {
            if (textoChar.length > i) {
                salida = salida + textoChar[i];
            } else {
                salida = salida + " ";
            }
        }
        return salida;
    }

    /**
     *
     * @param texto
     * @param tamano
     * @return salida
     */
    public static String completarNumero(String texto, Integer tamano) {
        String salida = "";
        char[] textoChar = texto.toCharArray();
        for (int i = 0; i < tamano; i++) {
            if (textoChar.length > i) {
                salida = salida + textoChar[i];
            } else {
                salida = "0" + salida;
            }
        }
        return salida;
    }

    /**
     *
     * @param number
     * @return number
     */
    public static BigDecimal getNumero(String number) {
        try {
            if (number == null) {
                return null;
            } else {
                String str = number.trim();
                if (str.equals(""))
                    return null;
                else
                    return new BigDecimal(number.trim());
            }
        } catch (Exception e) {
            return new BigDecimal(0);
        }
    }
    
    /**
     *
     * @param value
     * @return
     */
    public static Boolean isEmpty(Object value) {
        try {
            if (value == null) {
                return true;
            } else {
                if (value.toString()
                         .trim()
                         .equals("")) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
            return true;
        }
    }
    
    /**
     *
     * @param value
     * @return
     */
    public static String getValor(Object value) {
        try {
            if (value == null) {
                return "";
            } else {
                return value.toString();
            }
        } catch (Exception e) {
            return "";
        }
    }
    
    /**
     *
     * @param value
     * @return
     */
    public static String getDato(Object value) {
        String result = null;
        try {
            if (value != null) {
                result = value.toString();
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
        return result;
    }
    
    /**
     *
     * @param vlrDoub
     * @return
     */
    public static String convertDouble(Double vlrDoub) {
        try {
            Locale.setDefault(Locale.US);
            DecimalFormat num = new DecimalFormat("##.00");
            String res = num.format(vlrDoub);
            System.out.println("valor res: " + res);
            return res;
        } catch (Exception e) {
            return "0";
        }
    }

    /**
     * Metodo que retorna un valor y si genera nullpointerException retorna 0
     *
     * @param value
     * @return resultSalida
     */
    public static Integer convertNumber(Object value) {
        try {
            if (value == null || value.toString()
                                      .trim()
                                      .length() == 0) {
                return new Integer("0");
            } else {
                return new Integer(value.toString());
            }
        } catch (Exception e) {
            return new Integer("0");
        }
    }
    
    /**
     *
     * @param tp
     * @return
     */
    public static String getTpId(String tp) {
        String tpId = null;
        try {
            int idTp = Integer.parseInt(tp);
            switch (idTp) {
            case 1:
                tpId = "CC";
                break;
            case 2:
                tpId = "NI";
                break;
            case 3:
                tpId = "NE";
                break;
            case 4:
                tpId = "CE";
                break;
            default:
                tpId = "  ";
                break;
            }
        } catch (Exception e) {
            tpId = "  ";
        }
        return tpId;
    }
    
    /**
     *
     * @param mapa
     * @param serviceName
     * @param listName
     * @return
     * @throws Exception
     */
    public static String convertMapa2XML(Map<Integer, ArrayList<Values>> mapa, String serviceName,
                                      String listName) throws Exception {
        String xmlOutput;
        int arrayIndex = mapa.values().size();
        xmlOutput = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> \n";
        xmlOutput += "<" + serviceName + ">\n";
        for (int i = 0; i < arrayIndex; i++) {
            ArrayList<Values> inputs = mapa.get(i);
            xmlOutput += "\t<" + listName + ">\n";
            for (Values row : inputs) { 
                //xmlOutput +="\t\t<"+ tagName+"  name=\""+row.getName()+ "\"  value=\""+ row.getValue()  +"\"  />\n";
                if (row.getValue() != null && row.getValue() != "") {
                    xmlOutput += "\t\t <" + row.getName() + ">" + row.getValue() + "</" + row.getName() + ">\n";
                } else {
                    xmlOutput += "\t\t <" + row.getName() + " />\n";
                }
            }
            xmlOutput += "\t</" + listName + ">\n";
        }
        xmlOutput += "</" + serviceName + ">\n";
        return xmlOutput;
    }
    
    /**
     *
     * @return
     */
    public static Map<Integer, ArrayList<Values>> iniciaConversion() {
        Map<Integer, ArrayList<Values>> mapa = new HashMap<Integer, ArrayList<Values>>();
        int arrayIndex = 0;
        int index = 0;
        for (int i = 0; i <= 10; i++) {
            ArrayList<Values> inputs = new ArrayList<Values>();
            for (int v = 0; v <= 10; v++) {
                Values val = new Values();
                val.setValue("valor" + v);
                inputs.add(index, val);
                index++;
            }
            index = 0;
            mapa.put(arrayIndex, inputs);
            arrayIndex++;
        }
        for (int i = 0; i < arrayIndex; i++) {
            index = 0;
            ArrayList<Values> inputs = mapa.get(i);
            //for (String outputNames : scoreResult.getColumnNames().getName()) {
            for (int v = 0; v <= 10; v++) {
                Values val = inputs.get(index);
                val.setName("nombre" + v);
                inputs.set(index, val);
                index++;
            }
            mapa.put(i, inputs);
        }
        return mapa;
    }    
    
    /**
     *
     * @param mapa
     * @param key
     * @return
     */
    public static List buscarDatoXml( Map<Integer, ArrayList<Values>> mapa , String key){
        List<String> dato= new ArrayList<String>();
        mapa.forEach((index, listaVal) ->{ 
            listaVal.forEach((elemento) -> {
                if(elemento.getName().equalsIgnoreCase(key)){
                             dato.add(elemento.getValue());
                }
            });
         } );
        return dato;
    }
}
