package util;

public class TasasGeneric {
    
    private String monto;
    private String plazo;
    private String codigo;
    private String rango;
    private String codigoConvenio;
    private String codigoEstrategia;
    private String perfil;

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getMonto() {
        return monto;
    }

    public void setPlazo(String plazo) {
        this.plazo = plazo;
    }

    public String getPlazo() {
        return plazo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setRango(String rango) {
        this.rango = rango;
    }

    public String getRango() {
        return rango;
    }

    public void setCodigoConvenio(String codigoConvenio) {
        this.codigoConvenio = codigoConvenio;
    }

    public String getCodigoConvenio() {
        return codigoConvenio;
    }

 
    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public String getPerfil() {
        return perfil;
    }


    public void setCodigoEstrategia(String codigoEstrategia) {
        this.codigoEstrategia = codigoEstrategia;
    }

    public String getCodigoEstrategia() {
        return codigoEstrategia;
    }
}
