package util;

import java.io.Serializable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class ConvertXMLVO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String nameSpace;
    private String typeEncoding;
    private Class<?> rootElementClass;

    private Object objectFactory;
    private JAXBContext jaxbContext;
    private Unmarshaller unmarshaller;
    private Marshaller marshaller;

    /**
     * @return the nameSpace
     */
    public String getNameSpace() {
        return nameSpace;
    }

    /**
     * @param nameSpace
     *          the nameSpace to set
     */
    public void setNameSpace(String nameSpace) {
        this.nameSpace = nameSpace;
    }

    /**
     * @return the typeEncoding
     */
    public String getTypeEncoding() {
        if (this.typeEncoding == null) {
            this.typeEncoding = "UTF-8";
        }
        return typeEncoding;
    }

    /**
     * @param typeEncoding
     *          the typeEncoding to set
     */
    public void setTypeEncoding(String typeEncoding) {
        this.typeEncoding = typeEncoding;
    }

    /**
     * @return the objectFactory
     */
    public Object getObjectFactory() {
        return objectFactory;
    }

    /**
     * @param objectFactory
     *          the objectFactory to set
     */
    public void setObjectFactory(Object objectFactory) {
        this.objectFactory = objectFactory;
    }

    /**
     * @return the jaxbContext
     */
    public JAXBContext getJaxbContext() {
        return jaxbContext;
    }

    /**
     * @param jaxbContext
     *          the jaxbContext to set
     */
    public void setJaxbContext(JAXBContext jaxbContext) {
        if (jaxbContext != null) {
            try {
                this.setUnmarshaller(jaxbContext.createUnmarshaller());
                this.setMarshaller(jaxbContext.createMarshaller());
            } catch (Exception e) {
                System.out.println("error en la creacion del createUnmarshaller y createMarshaller: " +
                                   e.getMessage());
            }

        }
        this.jaxbContext = jaxbContext;
    }

    /**
     * @return the unmarshaller
     */
    public Unmarshaller getUnmarshaller() {
        return unmarshaller;
    }

    /**
     * @param unmarshaller
     *          the unmarshaller to set
     */
    private void setUnmarshaller(Unmarshaller unmarshaller) {
        this.unmarshaller = unmarshaller;
    }

    /**
     * @return the marshaller
     */
    public Marshaller getMarshaller() {
        return marshaller;
    }

    /**
     * @param marshaller
     *          the marshaller to set
     */
    private void setMarshaller(Marshaller marshaller) {
        this.marshaller = marshaller;
    }

    /**
     * @return the rootElementClass
     */
    public Class<?> getRootElementClass() {
        return rootElementClass;
    }

    /**
     * @param rootElementClass the rootElementClass to set
     */
    public void setRootElementClass(Class<?> rootElementClass) {
        this.rootElementClass = rootElementClass;
    }

    /* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */

    @Override
    public String toString() {
        return "ConvertXMLVO [nameSpace=" + nameSpace + ", typeEncoding=" + typeEncoding + ", rootElementClass=" +
            rootElementClass + ", objectFactory=" + objectFactory + ", jaxbContext=" + jaxbContext +
            ", unmarshaller=" + unmarshaller + ", marshaller=" + marshaller + "]";
    }
}
