package util;

public interface ErrorConstants {

    /**
     * Define el tipo de error de entrada
     */
    public static final String TYPE_ERROR_ERINP = "ERINP";

    public static final String TYPE_ERROR_ERINP1 = "ERINP1";

    /**
     * Define el tipo de error Exception
     */
    public static final String TYPE_ERROR_EREXP = "EREXP";

    /**
     * Define el tipo de error problemas en la sentencia SQL
     */
    public static final String TYPE_ERROR_ERSQL = "ERSQL";

    /**
     * Define el tipo de error relacionado con acceso a la BD
     */
    public static final String TYPE_ERROR_ERBDA = "ERBDA";

    /**
     * Define el tipo de error de invocaci&oacute;n remota
     */
    public static final String TYPE_ERROR_ERREM = "ERREM";

    /**
     * Define el tipo de error de invocaci&oacute;n remota
     */
    public static final String TYPE_ERROR_ERRGN = "ERRGN";

    /*
	 * Define error de los datos de entrada
	 */
    public static final String ERINP1 = "Mensaje de entrada nulo o vacio";
    public static final String ERINP2 = "La longitud del mensaje de entrada no es correcta";
    public static final String ERINP3 = "Dato de entrada nulo o vacio";
    public static final String ERINP4 = "Fecha vacia o formato erroneo";
    public static final String ERINP5 = "Error convirtiendo dato de entrada a numero";
    public static final String ERINP6 = "Estado de transaccion no valido";
    public static final String ERINP7 = "Estado de corresponsal no valido";
    public static final String ERINP8 = "Error convirtiendo hora habil";
    public static final String ERINP9 = "La causal tiene un estado vacio";
    public static final String ERINP10 = "Estado de PDS no valido";
    public static final String ERINP11 = "Estado de terminal no valido";
    public static final String ERINP12 = "Estado de canal no valido";

    public static final String ERINP13 = "Tipo cuenta comision es obligatorio para tipo de traslado(I)Interbancario";
    public static final String ERINP14 = "Codigo Banco es obligatorio para tipo de traslado(I)Interbancario";
    public static final String ERINP15 = "Tipo Id del titular es obligatorio para tipo de traslado(I)Interbancario";
    public static final String ERINP16 = "Numero Id del titular es obligatorio para tipo de traslado(I)Interbancario";
    public static final String ERINP17 = "Nombre del titular es obligatorio para tipo de traslado(I)Interbancario";
    public static final String ERINP18 = "Tipo Traslado debe ser, P: traslado propio I: traslado interbancario.";


    /**
     * Define error cuando ocurre una excepci&oacute;n
     */
    public static final String EREXP1 = "Excepcion inesperada";

    /**
     * Define error cuando ocurre una excepci&oacute;n
     */
    public static final String ERREM1 = "Error del servicio de comunicacion remoto";

    /*
	 * Define el error acceso a base de datos
	 */
    public static final String ERBDA1 = "Error al cerrar los recursos abiertos";
    public static final String ERBDA2 = "Error durante ejecuciom de sentencia SQL";
    public static final String ERBDA3 = "No es posible obtener conexion al pool de StarMaster";
    public static final String ERBDA4 = "Error cerrando la conexion de StarMaster";
    public static final String ERBDA5 = "No es posible obtener conexion al pool";
    public static final String ERBDA6 = "No es posible actualizar registro";
    public static final String ERBDA7 = "Error en la invocacion del procedimiento almacenado";

    /*
     * Define errores de reglas de negocio
     */
    public static final String ERRGN1 = "Cliente no existe";
    public static final String ERRGN2 = "No existen transacciones parametrizadas";
    public static final String ERRGN3 = "No hay datos para actualizar";
    public static final String ERRGN4 = "La transaccion ya esta deshabilitada";
    public static final String ERRGN5 = "La transaccion ya esta habilitada";
    public static final String ERRGN6 = "No existe el canal";
    public static final String ERRGN7 = "No existen transacciones";
    public static final String ERRGN8 = "Cuenta No Existe";
    public static final String ERRGN9 = "Cuenta esta Cancelada";
    public static final String ERRGN10 = "Cuenta esta Inactiva";
    public static final String ERRGN11 = "Cuenta esta Bloqueada";
    public static final String ERRGN12 = "Cuenta No puede ser de Pensionados";
    public static final String ERRGN13 = "Cuenta No puede ser Ahorro Programado";
    public static final String ERRGN14 = "Cuenta No puede ser Recaudadora";
    public static final String ERRGN15 = "Cuenta No puede ser Ahorro Contractual";
    public static final String ERRGN16 = "Cuenta No puede ser Cuenta AFC";
    public static final String ERRGN17 = "Cuenta No puede ser Cuenta AVC";
    public static final String ERRGN18 = "Cuenta No puede ser Cuenta Joven";
    public static final String ERRGN19 = "Cuenta No puede ser Cuenta CATS";
    public static final String ERRGN20 = "Cuenta No puede ser Cuenta Maestra";
    public static final String ERRGN21 = "Cuenta No puede ser Cuenta Bolsillo";
    public static final String ERRGN22 = "Cuenta Comision No Existe";
    public static final String ERRGN23 = "Cuenta Comision esta Cancelada";
    public static final String ERRGN24 = "Cuenta Comision esta Inactiva";
    public static final String ERRGN25 = "Cuenta Comision esta Bloqueada";
    public static final String ERRGN26 = "Cuenta Comision No puede ser de Pensionados";
    public static final String ERRGN27 = "Cuenta Comision No puede ser Ahorro Programado";
    public static final String ERRGN28 = "Cuenta Comision No puede ser Recaudadora";
    public static final String ERRGN29 = "Cuenta Comision No puede ser Ahorro Contractual";
    public static final String ERRGN30 = "Cuenta Comision No puede ser Cuenta AFC";
    public static final String ERRGN31 = "Cuenta Comision No puede ser Cuenta AVC";
    public static final String ERRGN32 = "Cuenta Comision No puede ser Cuenta Joven";
    public static final String ERRGN33 = "Cuenta Comision No puede ser Cuenta CATS";
    public static final String ERRGN34 = "Cuenta Comision No puede ser Cuenta Maestra";
    public static final String ERRGN35 = "Cuenta Comision No puede ser Cuenta Bolsillo";
    public static final String ERRGN36 = "No existe el sitio";
    public static final String ERRGN37 = "No existen terminales para el sitio";
    public static final String ERRGN38 =
        "La transaccion esta deshabilitada para el sitio y habilitada";
    public static final String ERRGN39 =
        "La transaccion esta habilitada para el sitio y deshabilitada";
    public static final String ERRGN40 = "El sitio se encuentra Cancelado";
    public static final String ERRGN41 = "La causal no corresponde al originador";
}
