 /**
  * 
  */
 package util;

 import java.io.ByteArrayInputStream;
 import java.io.ByteArrayOutputStream;
 import java.io.UnsupportedEncodingException;
 import java.lang.reflect.Method;

 import javax.xml.bind.JAXBContext;
 import javax.xml.bind.JAXBElement;
 import javax.xml.bind.JAXBException;
 import javax.xml.bind.Marshaller;

import org.pmw.tinylog.Logger;


/**
 * @author G905TEPR
 *
 */
 public class Conversion {


         private ConvertXMLVO xmlvo;

         public Conversion(ConvertXMLVO xmlvo) {
                 this.xmlvo = xmlvo;
                 iniciarStructurasConversion();
         }

         /**
          * METODO QUE SE ENCARGA DE INICIAR LAS ESTRUCTURAS DEL JAXBCONTEXT
          * YA SEA POR NAMESPACE O EL CLASS DEL ELEMENTO RAIZ
          */
         private void iniciarStructurasConversion() {
                 JAXBContext jaxbContext;
                 try {
                         if (this.xmlvo.getNameSpace() != null) {
                                 jaxbContext = JAXBContext.newInstance(this.xmlvo.getNameSpace());
                         } else {
                                 jaxbContext = JAXBContext.newInstance(this.xmlvo.getRootElementClass());
                         }
                         this.xmlvo.setJaxbContext(jaxbContext);
                 } catch (Exception e) {
                         System.out.println("Error Inesperado al iniciarStructurasConversion. " + e.getMessage());
                 }
         }

         /**
          * mETODO QUE SE ENCARGA DE CONVERTIR LA CADENA A OBJETO GENERADO A PARTIR DEL ESQUEMA ESQUEMA
          * @param msgXml
          * @return objectBean
          * @throws Exception
          */
         public Object StringXMLToBean(String msgXml) throws Exception {
                 ByteArrayInputStream input = null;
                 Exception exception = null;
                 Object objectBean = null;

                 try {
                         input = new ByteArrayInputStream(msgXml.getBytes(this.xmlvo.getTypeEncoding()));
                         
                         if (this.xmlvo.getNameSpace() != null) {
                                 objectBean = ((JAXBElement<?>) this.xmlvo.getUnmarshaller().unmarshal(input)).getValue();
                         } else {
                                 objectBean =  this.xmlvo.getUnmarshaller().unmarshal(input);
                         }
                 } catch (UnsupportedEncodingException e) {
                         exception = new Exception("10002 Encoding " + this.xmlvo.getTypeEncoding() + " No soportado", e);
                 } catch (JAXBException e) {
                         exception = new Exception("10002 VALOR INVALIDO", e);
                 } finally {
                         try {
                                 if (input != null) {
                                         input.close();
                                         input = null;// help GC
                                 }
                         } catch (Exception exc) {
                                 Logger.warn(exc, "Error cerrando input buffer " + exc.getMessage() );
                         }

                         if (exception != null) {
                                 throw exception;
                         }
                 }
                 return objectBean;
         }

         /**
          * mETODO QUE SE ENCARGA DE CONVERTIR EL OBJETO A CADENA
          * @param bean
          * @return result
          */
         public String BeanToStringXML(Object bean) {
                 JAXBElement<?> jaxbElement = null;
                 ByteArrayOutputStream output = new ByteArrayOutputStream();
                 String result;

                 try {
                         if (this.xmlvo.getNameSpace() != null) {
                                 jaxbElement = getJAXBElement(this.xmlvo.getObjectFactory(), bean);
                                 output.reset();
                                 this.xmlvo.getMarshaller().setProperty(Marshaller.JAXB_ENCODING, this.xmlvo.getTypeEncoding());
                                 this.xmlvo.getMarshaller().marshal(jaxbElement, output);
                         } else {
                                 this.xmlvo.getMarshaller().setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                                 this.xmlvo.getMarshaller().marshal(bean, output);
                         }                       
                         
                         result = output.toString();
                 } catch (Exception e) {
                         result = "ER";
                 }
                 return result;
         }

         /**
          * METODO ENCARGADO DE ASIGNAR LA INFORMACION AL OBJETO
          * @param oFactory
          * @param bean
          * @return jaxElement
          * @throws Exception
          */
         private JAXBElement<?> getJAXBElement(Object oFactory, Object bean) throws Exception {
                 Method[] ms = oFactory.getClass().getMethods();
                 for (int i = 0; i < ms.length; i++) {
                         Class<?>[] parametros = ms[i].getParameterTypes();
                         if (parametros != null && parametros.length == 1) {
                                 if (parametros[0].getName().equals(bean.getClass().getName())) {
                                         Method m = ms[i];
                                         JAXBElement<?> jaxElement = (JAXBElement<?>) m.invoke(oFactory, bean);
                                         return jaxElement;
                                 }
                         }
                 }
                 throw new Exception("10002 Error instanciando JAXBElement", (Exception) null);
         }

 }
