package util;

import error.ExceptionStarGeneric;

import java.util.Calendar;
import java.util.GregorianCalendar;


import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.pmw.tinylog.Logger;


public class ConversionTrama {
    private StringBuilder sbFiller = new StringBuilder();

    public ConversionTrama() {
        super();
    }

    public void crearRespuestaError(String trama) throws ExceptionStarGeneric {
        String codigo = trama.substring(5, 10);
        String descripcion = trama.substring(10);
        String tipo = trama.substring(2, 5);
        throw new ExceptionStarGeneric(tipo, codigo, descripcion);
    }

    public String filler(String value, int size) throws ExceptionStarGeneric {
        if (value == null)
            value = "";

        sbFiller.delete(0, sbFiller.length());
        sbFiller.append(value);

        int l = size - value.length();

        if (l == 0) {          
            Logger.debug("Valor inicial = [" + value + "]= Valor final");
            return value;
        }

        if (l < 0) {
            throw new ExceptionStarGeneric("10022", "Valor = " + value + " Lon = " + value.length() + " > " + size,
                                           (Exception)null);
        }
        for (int i = 0; i < l; i++) {
            sbFiller.append(" ");
        }

        String valorFinal = sbFiller.toString();
          Logger.debug("Valor inicial = [" + value + "] Valor final = [" + valorFinal + "]");      
          return valorFinal;
    }

    /*xml calendar grerorian*/

    public XMLGregorianCalendar stringToCalendar(String strFecha) throws DatatypeConfigurationException {

        if (strFecha == null) {
            return null;
        }

        GregorianCalendar gcal = new GregorianCalendar();
        gcal.set(Integer.valueOf(strFecha.substring(0, 4)), Integer.valueOf(strFecha.substring(4, 6)) - 1,
                 Integer.valueOf(strFecha.substring(6, 8)), 0, 0, 0);
        XMLGregorianCalendar xgcal = DatatypeFactory.newInstance().newXMLGregorianCalendar(gcal);
        return xgcal;
    }

    public String calendarToString(XMLGregorianCalendar xgcal) {
        if (xgcal == null) {
            return null;
        }

        Calendar calendar = xgcal.toGregorianCalendar();
        return (fechaLongtoString(calendar));
    }

    public String fechaLongtoString(Calendar calendar) {
        String fecha = null;
        if (calendar == null) {
            calendar = Calendar.getInstance();
        }
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        fecha = year + "";
        if (month <= 9) {
            fecha = fecha + "0" + month;
        } else {
            fecha = fecha + month;
        }

        if (day <= 9) {
            fecha = fecha + "0" + day;
        } else {
            fecha = fecha + day;
        }
        Logger.debug("Fecha string = " + fecha);
        return fecha;
    }
}
