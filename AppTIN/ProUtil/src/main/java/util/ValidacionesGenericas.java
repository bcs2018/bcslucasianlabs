package util;

public class ValidacionesGenericas {
    public ValidacionesGenericas() {
        super();
    }

    public boolean validacionGenerica(String msgIn, int longitud) {
        if (msgIn == null || msgIn.trim().equals("")) {
            return true;
        } else if (msgIn.length() != longitud) {
            return true;
        } else {
            return false;
        }
    }
}
