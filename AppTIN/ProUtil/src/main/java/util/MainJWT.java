/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;

/**
 * Clase principal utilizada para la gestion de tokens en APPTIN.
 *
 * @author
 */
public class MainJWT extends JWT {

    public static String createToken(final String secret) {
        String token = null;
        try {
            Algorithm algorithm = Algorithm.HMAC256(secret);
            token = JWT.create()
                    .withIssuer("auth0")
                    .sign(algorithm);
            System.out.println("Token generado es " + token);
            //Token generado es eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhdXRoMCJ9.izVguZPRsBQ5Rqw6dhMvcIwy8_9lQnrO3vpxGwPCuzs
        } catch (JWTVerificationException exception) {
            throw exception;
        }
        return token;
    }

    public static String createToken(final RSAPublicKey publicKey, final RSAPrivateKey privateKey) {
        String token = null;
        try {
            Algorithm algorithm = Algorithm.RSA256(publicKey, privateKey);
            token = JWT.create()
                    .withIssuer("auth0")
                    .sign(algorithm);
            System.out.println("Token generado es " + token);
            //Token generado es eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhdXRoMCJ9.izVguZPRsBQ5Rqw6dhMvcIwy8_9lQnrO3vpxGwPCuzs
        } catch (JWTVerificationException exception) {
            exception.printStackTrace();
        }
        return token;
    }

    public static DecodedJWT verifyToken(final String token, final String secret) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(secret);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer("auth0")
                    .build(); //Reusable verifier instance
            DecodedJWT jwt = verifier.verify(token);
            System.out.println("se genera un objeto DecodedJWT: " + jwt);
            return jwt;

        } catch (JWTVerificationException exception) {
            System.out.println("Error en la validacion del token");
        }
        return null;
    }

    public static void verifyToken(final String token, RSAPublicKey publicKey, RSAPrivateKey privateKey) {
        try {

            Algorithm algorithm = Algorithm.RSA256(publicKey, privateKey);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer("auth0")
                    .build(); //Reusable verifier instance
            DecodedJWT jwt = verifier.verify(token);
            System.out.println("se genera un objeto DecodedJWT: " + jwt);

        } catch (JWTVerificationException exception) {
            System.out.println("Error en la validacion del token");
        }
    }

    public static void loadKeys(String keyPath) throws InvalidKeySpecException, NoSuchAlgorithmException, InvalidKeyException, IOException, SignatureException {
        //TODO 
        
    }

    public static DecodedJWT decodeToken(String token) {
        DecodedJWT jwt = null;
        try {
            jwt = JWT.decode(token);
        } catch (JWTDecodeException exception) {
            //Invalid token
        }
        return jwt;
    }

    public static void main(String arg[]) throws NoSuchAlgorithmException, NoSuchProviderException, FileNotFoundException, IOException {
        //Funcionalidad Creacion token
        //1. Se recibe un secret id(o par de llaves) por parte de ACH
        //FIXME Revisar si se va a enviar llaves o secret ID, por parte de ACH
        String secret = "Lucasian2018";//Se recibe el secret por parte de ACH
        //2. Se valida si el secret id por parte de ACH es valido en el archivo
        //de propiedades
        //3. Si se supera las validaciones entonces se genera un token
        String token = MainJWT.createToken(secret);
        //Verificar token
        token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJhdXRoMCJ9.izVguZPRsBQ5Rqw6dhMvcIwy8_9lQnrO3vpxGwPCuzs";
        secret = "secret";
        MainJWT.verifyToken(token, secret);

    }

}
