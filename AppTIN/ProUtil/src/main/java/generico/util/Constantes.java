package generico.util;


public class Constantes {

    public static final String SQL_UPDATE = "sql.update";
    public static final String PARAMETER_UPDATE = "parameter.update";
    public static final String ERROR_PATH = "ERROR_PATH";
    public static final String PROPS_PATH = "PROPS_PATH";
    public static final String IDIOMA = "idioma";
    public static final String OLD_FORMAT = "old.format";
    public static final String NEW_FORMAT = "new.format";
    public static final String ACTUAL_FORMAT = "actual.format";
    
    public static final String ERROR_TYPE_RES = "error.type.res";

    public static final String URL_AUTH = "URL_AUTH";
    public static final String URL_INICIARTRANSFERCENCIA = "URL_INICIARTRANSFERCENCIA";
    public static final String URL_LISTARTRANSFERCENCIA = "URL_LISTARTRANSFERCENCIA";
    public static final String URL_ACEPTARTRANSFERCENCIA = "URL_ACEPTARTRANSFERCENCIA";

    
    public static final String GRANTTYPE = "GRANT_TYPE";
    public static final String CLIENTID = "CLIENT_ID";
    public static final String CLIENTSECRET = "CLIENT_SECRET";

    public static final String TIMEOUTCX = "cliente.connectio.timeout";
    public static final String TIMEOUTRS = "cliente.servicio.timeout";
    

    /*
     * Define Cosntantes Genericas
     */
    
    static final public String ER = "ER";
    static final public String OK = "OK";
    //Tipos de ERROR
    static final public String DBA = "BDE";
    static final public String COM = "COM";
    static final public String VAL = "VAL";
    static final public String OTR = "OTR";

    public static final String ERROR_SERVICIO_TEXTO="error.servicio.texto";
    public static final String ERROR_SERVICIO_CODIGO="error.servicio.codigo";
    
    
    public static final String ERROR_GENERICO_TEXTO= "error.generico.texto";
    public static final String ERROR_GENERICO_CODIGO= "error.generico.codigo";
    
    public static final String ERROR_TIMEOUT_TEXTO= "error.timeout.texto";
    public static final String ERROR_TIMEOUT_CODIGO= "error.timeout.codigo";
    
    public static final String ERROR_COMUNICACION_TEXTO= "error.comunicacion.texto";
    public static final String ERROR_COMUNICACION_CODIGO= "error.comunicacion.codigo";
    
    public static final String ERROR_GEN_CONS_TEXTO= "error.gen.cons.texto";
    public static final String ERROR_GEN_CONS_CODIGO= "error.gen.cons.codigo";
    
    public static final String ERROR_AUTHEN_TEXTO= "error.authen.texto";
    public static final String ERROR_AUTHEN_CODIGO= "error.authen.codigo";
    
    public static final String ERROR_TRANSFORMACION_ENTRADA_TEXTO= "error.transformacion.texto";
    public static final String ERROR_TRANSFORMACION_ENTRADA_CODIGO= "10001";
    
    public static final String ERROR_BD_TEXTO="error.bd.texto";
    public static final String ERROR_BD_CODIGO="error.bd.codigo";

    
    public static final String ERROR_TYPE_BD="error.type.bd";
    public static final String ERROR_TYPE_MW="error.type.mw";
    public static final String ERROR_TYPE_DAT="error.type.dat";
    public static final String ERROR_TYPE_WS="error.type.ws";
    

    /*constantes de data-source de STAR4U*/
    public static final String JNDIBD_S4U = "JNDIBD_S4U";

    public static final String LLAVE_PUBLICA = "llave.publica";

    public static final String LLAVE_PRIVADA = "llave.privada";





    public static boolean isSimpleObject(Object value) {
        if (value == null)
            return true; //esto es un error

        if (value instanceof java.lang.String)
            return true;

        if (value instanceof java.math.BigInteger)
            return true;

        if (value instanceof java.math.BigDecimal)
            return true;

        if (value instanceof java.sql.Timestamp)
            return true;

        if (value instanceof java.sql.Date)
            return true;

        if (value instanceof javax.xml.datatype.XMLGregorianCalendar)
            return true;

        if (value instanceof java.lang.Integer)
            return true;

        if (value instanceof java.lang.Boolean)
            return true;

        if (value instanceof java.lang.Character)
            return true;

        if (value instanceof java.lang.Byte)
            return true;

        if (value instanceof java.lang.Short)
            return true;

        if (value instanceof java.lang.Long)
            return true;

        if (value instanceof java.lang.Float)
            return true;

        if (value instanceof java.lang.Double)
            return true;

        return false;
    }


}
