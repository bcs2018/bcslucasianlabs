package generico.util;


import error.DatosEntradaException;

import java.io.ByteArrayOutputStream;

import java.math.BigDecimal;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.pmw.tinylog.Logger;

/**
 * La clase <code>Utils</code> provee m&eacute;todos utilitarios para todas la
 * aplicaci&oacute;n.
 *
 */
public class Utils {


    public static final String DATE_FORMAT_1 = "yyyyMMdd";
    public static final String DATE_FORMAT_2 = "HH:MM";
    public static final String DATE_FORMAT_3 = "dd/MM/yyyy";


    private static JAXBContext jaxbContext;

    /**
     * @param fecha
     * @param formato
     * @throws DatosEntradaException
     */
    public static void validarFecha(String fecha, String formato) throws DatosEntradaException {
        if (fecha.trim().equals("")) {
            throw new DatosEntradaException("ERINP3", "ERINP", "3");
        } else {
            string2Calendar(fecha, formato);
        }
    }

    public static String calendar2String(Calendar calendar, String pattern) {
        SimpleDateFormat formateDate = new SimpleDateFormat(pattern);

        return formateDate.format(calendar.getTime());
    }

    public static String formatDate(String fecha, String formatoInicio,
                                    String formatoFinal) throws DatosEntradaException {
        try {
            SimpleDateFormat formateDate = new SimpleDateFormat(formatoInicio);

            Date d = formateDate.parse(fecha);

            formateDate = new SimpleDateFormat(formatoFinal);

            return formateDate.format(d);
        } catch (ParseException e) {
            throw new DatosEntradaException("ERINP4", "ERINP", "4");
        }
    }

    public static Calendar string2Calendar(String fecha, String formato) throws DatosEntradaException {
        Calendar c = null;

        try {
            SimpleDateFormat formateDate = new SimpleDateFormat(formato);

            Date d = formateDate.parse(fecha);

            c = Calendar.getInstance();
            c.setTime(formateDate.parse(formateDate.format(d)));
        } catch (ParseException e) {
            throw new DatosEntradaException("ERINP4", "ERINP", "4");
        }
        return c;
    }

    public static Integer string2Int(String cadena) throws DatosEntradaException {
        try {
            return Integer.parseInt(cadena);
        } catch (Exception e) {
            throw new DatosEntradaException("ERINP5", "ERINP", "5");
        }
    }

    public static Long string2Long(String cadena) throws DatosEntradaException {
        try {
            return Long.parseLong(cadena);
        } catch (Exception e) {
            throw new DatosEntradaException("ERINP5", "ERINP", "5");
        }
    }

    public static void validarDatos(String[] datos) throws DatosEntradaException {
        for (String dato : datos) {
            if (dato.equals("")) {
                throw new DatosEntradaException("ERINP3", "ERINP", "3");
            }
        }
    }

    /**
     * Funci&oacute;n para alinear un campo tipo String a la izquierda
     *
     * @param s Campo a formatear
     * @param l Longitud total del campo
     * @return String Campo formateado
     */
    public static String pad(String s, int l) {
        String blancos = "             ";

        for (int i = 1; i < 1000; i++)
            blancos = blancos + "  ";

        if (s == null)
            return blancos.substring(0, l);
        else
            return (s + blancos).substring(0, l);
    }

    /**
     * Funci&oacute;n para alinear un campo tipo Integer a la derecha, y
     * completarlo con ceros
     *
     * @param n Campo a formatear
     * @param l Longitud del campo
     * @return String Campo formateado
     */
    public static String pad(Integer n, int l) {
        String ceros = "0000000000000000";

        return (ceros + n).substring((16 + n.toString().length()) - l);
    }

    private static void createJAXBContext(String rootPath) throws JAXBException {
        if (jaxbContext == null) {
            jaxbContext = JAXBContext.newInstance(rootPath);
        }
        return;
    }

    public static String object2XML(Object bean, String schema) {
        String response = "";
        try {
            createJAXBContext(schema);

            ByteArrayOutputStream output = new ByteArrayOutputStream();

            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.marshal(bean, output);

            response = output.toString();
        } catch (Exception e) {
            Logger.error(e, "Error inesperado en Object To XML ");
            response =
                    "<?xml version=\"1.0" + "encoding=\"UTF-8" + "standalone=\"yes" + "?>" + "<SalMovCBXNumCuenta xmlns=\"MovimientosCB" +
                    ">" + "<respuesta>" + "<TipoRespuesta>ER</TipoRespuesta>" + "<RespuestaError>" +
                    "<TipoError>INP</TipoError>" + "<CodigoError>00</CodigoError>" +
                    "<DescripcionError>Error generando string XML</DescripcionError>" + "</RespuestaError>" +
                    "</respuesta>" + "</SalMovCBXNumCuenta>";
        }
        return response;
    }

    public static String obtenerFecha(String format) {
        Date date = new Date();
        DateFormat hourdateFormat = new SimpleDateFormat(format);
        return hourdateFormat.format(date);
    }

    /**
     *
     * @param texto
     * @param tama�o
     * @return salida
     */
    public static String completarTexto(String texto, Integer tamano) {
        String salida = "";
        if (texto == null) {
            texto = "";
        }
        char[] textoChar = texto.toCharArray();
        for (int i = 0; i < tamano; i++) {
            if (textoChar.length > i) {
                salida = salida + textoChar[i];
            } else {
                salida = salida + " ";
            }
        }
        return salida;
    }

    /**
     *
     * @param texto
     * @param tama�o
     * @return salida
     */
    public static String completarNumero(String texto, Integer tamano) {
        String salida = "";
        char[] textoChar = texto.toCharArray();
        for (int i = 0; i < tamano; i++) {
            if (textoChar.length > i) {
                salida = salida + textoChar[i];
            } else {
                salida = "0" + salida;
            }
        }
        return salida;
    }



    /**
     * Metodo que retorna un valor y si genera nullpointerException retorna 0
     *
     * @param value
     * @return resultSalida
     */
    public static Integer convertNumber(Object value) {
        try {
            if (value == null || value.toString().trim().length() == 0) {
                return new Integer("0");
            } else {
                return new Integer(value.toString());
            }
        } catch (Exception e) {
            return new Integer("0");
        }
    }
    /**
     *
     * @param number
     * @return number
     */
    public static BigDecimal getNumero(String number) {
        try {
            if (number == null) {
                return null;
            } else {
                String str = number.trim();
                if (str.equals(""))
                    return null;
                else
                    return new BigDecimal(number.trim());
            }
        } catch (Exception e) {
            return new BigDecimal(0);
        }
    }

    public static Boolean isEmpty(Object value) {
        try {
            if (value == null) {
                return true;
            } else {
                if (value.toString().trim().equals("")) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
            return true;
        }
    }

    public static String getValor(Object value) {
        try {
            if (value == null) {
                return "";
            } else {
                return value.toString();
            }
        } catch (Exception e) {
            return "";
        }
    }

    public static String convertDouble(Double vlrDoub) {
        try {
            Locale.setDefault(Locale.US);
            DecimalFormat num = new DecimalFormat("##.00");
            String res = num.format(vlrDoub);
            System.out.println("valor res: " + res);
            return res;
        } catch (Exception e) {
            return "0";
        }
    }

    public static String getTpId(String tp) {
        String tpId = null;
        try {
            int idTp = Integer.parseInt(tp);
            switch (idTp) {
            case 1:
                tpId = "CC";
                break;
            case 2:
                tpId = "NI";
                break;
            case 3:
                tpId = "NE";
                break;
            case 4:
                tpId = "CE";
                break;
            default:
                tpId = "  ";
                break;
            }
        } catch (Exception e) {
            tpId = "  ";
        }
        return tpId;
    }
    
    public static String getCodTpId(String tp) {
        String tpId = null;
        try {
            switch (tp) {
            case "CC":
                tpId = "1";
                break;
            case "NI":
                tpId = "2";
                break;
            case "NE":
                tpId = "3";
                break;
            case "CE":
                tpId = "4";
                break;
            default:
                tpId = " ";
                break;
            }
        } catch (Exception e) {
            tpId = " ";
        }
        return tpId;
    }
}
