package common;

import co.com.bcs.tin.xsd.InitiateTransferEntType;

import co.com.bcs.tin.xsd.InitiateTransferSalType;

import com.google.gson.Gson;

import generico.util.Constantes;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;

import org.pmw.tinylog.Logger;

import util.PropertiesLoader;

public class Transformation {
    public static String toJson(InitiateTransferEntType iniciarTransferenciaType) throws Exception {
        String response = null;
        try {
            Gson gson = new Gson();
            response = gson.toJson(iniciarTransferenciaType);
            System.out.println(response);
            return response;
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e+"Error inesperado en la conversion al objeto toJson: ");
            throw new Exception("ER-MW: Error al transformar la solicitud",e);
        }
    }

    public static InitiateTransferSalType toObject(String jsonObject) throws Exception {
        InitiateTransferSalType response = new InitiateTransferSalType();
        try {
            Gson gson = new Gson();
            response = gson.fromJson(jsonObject, InitiateTransferSalType.class);
            return response;
        } catch (Exception e) {
            System.out.println(e+"Error inesperado en la conversion al objeto toObject");
            throw new Exception("ER-MW: Error al procesar la respuesta",e);
        }
    }
    
    public String convierteFecha(String date ) {
            final String OLD_FORMAT = PropertiesLoader.getProperty(Constantes.OLD_FORMAT);
            final String NEW_FORMAT =  PropertiesLoader.getProperty(Constantes.NEW_FORMAT);
            String newDateString = "";
            SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
            Date d;
            try {
                d = sdf.parse(date);
                sdf.applyPattern(NEW_FORMAT);
                newDateString = sdf.format(d);
                System.out.println("Fecha anterior: " + date + " Fecha cambiada: " + newDateString);
            } catch (ParseException e) {
                System.out.println(e+ "Error transformando fecha");
            }
            return newDateString;
        }

        public String convierteFechaWll11(String date ) {
            final String OLD_FORMAT = PropertiesLoader.getProperty(Constantes.NEW_FORMAT); 
            final String NEW_FORMAT = PropertiesLoader.getProperty(Constantes.OLD_FORMAT);
            String newDateString = "";
            SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
            Date d;
            try {
                d = sdf.parse(date);
                sdf.applyPattern(NEW_FORMAT);
                newDateString = sdf.format(d);
                Logger.info("Fecha anterior: " + date + " Fecha cambiada: " + newDateString);
            } catch (ParseException e) {
                Logger.error(e, "Error transformando fecha");
            }
            return newDateString;
        }

        public String getFechaHoraActual() {
            DateFormat dateFormat = new SimpleDateFormat(PropertiesLoader.getProperty(Constantes.ACTUAL_FORMAT));
            Date date = new Date();
            String dateTime = dateFormat.format(date);
            System.out.println("FechaHora actual: " + dateTime);
            return dateTime;
        }
}
