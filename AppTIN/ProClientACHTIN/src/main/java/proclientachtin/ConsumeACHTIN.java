package proclientachtin;

import auth.RequestType;
import auth.ResponseType;

import co.com.bcs.tin.xsd.InitiateTransferEntType;
import co.com.bcs.tin.xsd.InitiateTransferSalType;

import com.google.gson.Gson;

import com.google.gson.GsonBuilder;
/***
import com.minka.IouUtil;
import com.minka.KeyPairHolder;
import com.minka.wallet.Address;
import com.minka.wallet.IOU;

import com.minka.wallet.IouParamsDto;

import com.minka.wallet.SignatureDto;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;**/

import common.Transformation;

import error.ExceptionStarGeneric;

import generico.util.Constantes;

import java.math.BigDecimal;

import java.net.SocketTimeoutException;

import java.security.PrivateKey;

import java.sql.Timestamp;

import java.util.Calendar;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import java.util.prefs.Preferences;

//import javax.ws.rs.core.HttpHeaders;

//import org.apache.http.HttpStatus;

//import org.pmw.tinylog.Logger;


import types.json.Identidad;

import util.PropertiesLoader;
import util.Utils;


public class ConsumeACHTIN {

   /** public InitiateTransferSalType requestValidationInitiateTransfer(InitiateTransferEntType request) throws Exception {

        InitiateTransferSalType output = null;
        int timeOutcx = Utils.convertNumber(PropertiesLoader.getProperty(Constantes.TIMEOUTCX));
        int timeOutRs = Utils.convertNumber(PropertiesLoader.getProperty(Constantes.TIMEOUTRS));
        Identidad identidad = null;
        String url = PropertiesLoader.getProperty(Constantes.URL_INICIARTRANSFERCENCIA);
        Transformation tr = new Transformation();
        try {
            Logger.info("inicia busqueda de encabezado");

            //SE SETEAN LOS DATOS POR AHORA LOS QUEMAMOS
            request = TestJson.getData();

            Logger.info("Paso 1 inicia porceso auth");
            authenticationTIN();
            identidad = OAuthConstrol();
            Logger.info("Access_Token: " + identidad.getTocken());
            Logger.info("URL Iniciar Transferencia: " + url);
            Client client = Client.create();
            client.setConnectTimeout(timeOutcx);
            client.setReadTimeout(timeOutRs);
            Logger.info("URL_AUTH: " + url);
            WebResource webResource = client.resource(url);
            Logger.info("request webResource----------- ");

            // Obteniendo la respusta del cliente. ---> 20180716
            Gson gsonObj = new Gson();
            String jsonStr = gsonObj.toJson(request);
            Logger.info("JSON REQUEST: " + jsonStr);

            // TODO Pediente que ACH aclare que datos especifico espera en el "header"
            ClientResponse responseQuery = webResource.header("Accept-Encoding", "gzip,deflate")
                                                      .header("Content-Type", "application/json")
                                                      .header(HttpHeaders.AUTHORIZATION, identidad.getTocken())
                                                      .post(ClientResponse.class, jsonStr);

            Logger.info("STATUS query: " + responseQuery.toString());

            if (responseQuery == null) {
                Logger.error("Error en respuesta de ws (consulta): " + responseQuery);
                throw new RuntimeException("Failed : HTTP error code : " + responseQuery);
            }

            //Ini TODO
            String jsonRes = responseQuery.getEntity(String.class);
            Logger.info("Json: " + jsonRes);

            if (responseQuery.getStatus() >= HttpStatus.SC_BAD_REQUEST) {
                throw new ExceptionStarGeneric(responseQuery.getStatus() + ":" + responseQuery.getStatusInfo(),
                                               PropertiesLoader.getProperty(Constantes.ERROR_TYPE_WS),
                                               String.valueOf(responseQuery.getStatus()));
            }
        } catch (ClientHandlerException ex) {
            Logger.error(ex, " Error de comunicacion con tin requestValidationConsTIN(): ");
            if (ex.getCause() instanceof SocketTimeoutException) {
                throw new ExceptionStarGeneric(PropertiesLoader.getProperty(Constantes.ERROR_TIMEOUT_TEXTO),
                                               PropertiesLoader.getProperty(Constantes.ERROR_TYPE_RES),
                                               PropertiesLoader.getProperty(Constantes.ERROR_TIMEOUT_CODIGO));
            }
            throw new ExceptionStarGeneric(PropertiesLoader.getProperty(Constantes.ERROR_COMUNICACION_TEXTO),
                                           PropertiesLoader.getProperty(Constantes.ERROR_TYPE_RES),
                                           PropertiesLoader.getProperty(Constantes.ERROR_COMUNICACION_CODIGO));
        } catch (error.ExceptionStarGeneric e) {
            Logger.error(e, "requestValidationInitiateTransfer: Error inesperado al llamar el servicio");
            throw e;
        } catch (RuntimeException ex) {
            Logger.error(ex, "Error runtime request servicio TIN en ACH: ");
            throw new ExceptionStarGeneric("Error en consumo de web service de TIN ACH: " + ex.getMessage(),
                                           PropertiesLoader.getProperty(Constantes.ERROR_TYPE_MW), "10000");
        } catch (Exception e) {
            Logger.error(e, " Error ACH ");
            throw new ExceptionStarGeneric("Error inesperado al tratar de consumir web service de ACH",
                                           PropertiesLoader.getProperty(Constantes.ERROR_TYPE_WS), "10001");
        }

        return output;
    }


    private ResponseType authenticationTIN() throws ExceptionStarGeneric {
        System.out.println("*************************Inicia Autenticacion*****************************");
        RequestType request = new RequestType();
        request.setGrantType(PropertiesLoader.getProperty(Constantes.GRANTTYPE));
        request.setClientId(PropertiesLoader.getProperty(Constantes.CLIENTID));
        request.setClientSecret(PropertiesLoader.getProperty(Constantes.CLIENTSECRET));
        ResponseType responseTokenAutenti = null;

        try {
            //Agregar los queryParams ---20180716
            Client client = Client.create();
            System.out.println("URL_AUTH: " + PropertiesLoader.getProperty(Constantes.URL_AUTH));
            WebResource webResource = client.resource(PropertiesLoader.getProperty(Constantes.URL_AUTH));
            System.out.println("request webResource----------- ");
            // Obteniendo la respusta del cliente. ---20180309
            String requestString = "";
            try {
                Gson gson = new Gson();
                requestString = gson.toJson(request);
            } catch (Exception e) {
                System.out.println("Error inesperado en la conversion al objeto toJson: " + e);
                requestString = null;
            }
            ClientResponse response = webResource
                // .header("Accept", "application/json")
                .header("Content-Type", "application/x-www-form-urlencoded")
                .entity(requestString)
                .post(ClientResponse.class);

            System.out.println("status Auth: " + response.getStatus() + " Desc_Auth: " + response.getStatusInfo());
            //Validando la Respueta de la solicitud hacia ACH ---20180716

            if (response.getStatus() != HttpStatus.SC_OK) {
                System.out.println("Error en respuesta de WS ACH_TIN (authenticationTIN): " + response.getStatus());
                throw new ExceptionStarGeneric(response.getStatus() + ":" + response.getStatusInfo(), "WST",
                                               String.valueOf(response.getStatus()));

            }


            System.out.println("Autenticacion realizada Exitosamente...");
        } catch (ClientHandlerException ex) {
            System.out.println(ex + " Error en metodo de Autenticacion - TimeOut: ");
        }
        System.out.println("***************************************Finaliza Autenticacion***************************************");
        return responseTokenAutenti;
    }

    public Identidad OAuthConstrol() throws ExceptionStarGeneric {
        Identidad identidad = getPreferences();
        //Si no existe una session la inicia
        Logger.info("Identidad ClientId:" + identidad.getClientId());

        Logger.info("******Entra a autenticar");
        ResponseType responseType = authenticationTIN();
        setPreferences(responseType);
        identidad = getPreferences();

        //setea la clase Identidad
        Timestamp tiempoFinal = adicionaTiepo(identidad.getFechaHora(), identidad.getExpireIn());
        Logger.info("Sesion expira:" + tiempoFinal);


        return identidad;
    }

    public Timestamp adicionaTiepo(String timestamp, String expireIn) {
        Timestamp time = Timestamp.valueOf(timestamp);
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(time.getTime());
        cal.add(Calendar.SECOND, Integer.parseInt(expireIn));
        Timestamp timestampNew = new Timestamp(cal.getTime().getTime());
        return timestampNew;
    }

    private void setPreferences(ResponseType responseType) {
        Preferences prefs = Preferences.userNodeForPackage(ConsumeACHTIN.class);
        prefs.put("tocken", responseType.getTokenType() + " " + responseType.getIdToken());
        prefs.put("refreshTocken", responseType.getRefreshToken());
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        prefs.put("fechaHora", String.valueOf(timestamp));
        prefs.put("clientId", responseType.getClientId());
        prefs.put("expireIn", responseType.getExpireIn() + "");

    }

    private Identidad getPreferences() {
        Identidad identidad = new Identidad();
        Preferences prefs = Preferences.userNodeForPackage(ConsumeACHTIN.class);
        if (null != prefs.get("tocken", null)) {
            identidad.setTocken(prefs.get("tocken", null));
        }
        if (null != prefs.get("refreshTocken", null)) {
            identidad.setRefreshTocken(prefs.get("refreshTocken", null));
        }
        if (null != prefs.get("fechaHora", null)) {
            identidad.setFechaHora(prefs.get("fechaHora", null));
        }
        if (null != prefs.get("clientId", null)) {
            identidad.setClientId(prefs.get("clientId", null));
        }
        if (null != prefs.get("expireIn", null)) {
            identidad.setExpireIn(prefs.get("expireIn", "0"));
        }
        return identidad;
    }
    
    private void getIOU() throws Exception{
        
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.setPrettyPrinting().create();
        KeyPairHolder sourcekeyPairHolder = new KeyPairHolder();
        KeyPairHolder.KeyPairHolderDto keypairDto = sourcekeyPairHolder.getDtoForJson();
        System.out.println(gson.toJson(keypairDto));
        System.out.println("Now we generate an address");

        Address address = new Address(keypairDto.getPublico());
        String sourceAddress = address.getValue();
        System.out.println("addressGenerated : " + sourceAddress);

        IouParamsDto iouParamsDto;
        String domain = "www.bancocajasocial.com";

        String source = "wKiesuSseKmFND8bFhcxZ4VedjbgSucNf6";
        String target = "wRtHf9kgiLxjSuyeEqhkfQB45PoYmMfbZ3";
        String symbol = source;
        String random = "2eeff2202dc8c7f1db50";

        System.out.println("source:" + source);
        BigDecimal amount = new BigDecimal(100000);
        BigDecimal credit = new BigDecimal(0);
        System.out.println("We convert the java date to ISO FORMAT");
        String expiry = IouUtil.convertToIsoFormat(new Date());

        iouParamsDto = new IouParamsDto(domain,
                source, target, amount, credit,
                symbol, random, null, expiry);

        IouUtil iouUtil = new IouUtil();
        System.out.println("Here we write the claims");
        IOU theIou = iouUtil.write(iouParamsDto);

        Map<PrivateKey, SignatureDto> signaturePairs = new HashMap<>();
        signaturePairs.put(sourcekeyPairHolder.getSecret(), sourcekeyPairHolder.getBasicSignatureDto(sourceAddress));
        System.out.println("In order to sign we use the Key,value = PrivateKeys, SignatureDto");
        theIou.sign(signaturePairs);

        System.out.println("Printing pretty JSON ");
        System.out.println(theIou.toPrettyJson());
        System.out.println("Printing Raw JSON ");
        System.out.println(theIou.toRawJson());

        System.out.println("Printing pretty JSON for the FORMAT V022");
        //System.out.println(theIou.toPrettyJsonV022());
        System.out.println("Printing raw JSON for the FORMAT V022");
        //System.out.println(theIou.toRawJsonV022());
    
        
    }**/

}
