package proclientachtin;

import co.com.bcs.tin.xsd.AccountTypeType;
import co.com.bcs.tin.xsd.AmountType;
import co.com.bcs.tin.xsd.ClearingSystemType;
import co.com.bcs.tin.xsd.ContactDetailsType;
import co.com.bcs.tin.xsd.CreditTransferTXInformationType;
import co.com.bcs.tin.xsd.DebtorAccountType;
import co.com.bcs.tin.xsd.DebtorAgentType;
import co.com.bcs.tin.xsd.DeviceType;
import co.com.bcs.tin.xsd.FinInstitutionIdentificationType;
import co.com.bcs.tin.xsd.GeolocationType;
import co.com.bcs.tin.xsd.GroupHeaderType;
import co.com.bcs.tin.xsd.IOUType;
import co.com.bcs.tin.xsd.ISO20022Type;
import co.com.bcs.tin.xsd.Identification;
import co.com.bcs.tin.xsd.IdentificationType;
import co.com.bcs.tin.xsd.InfoClientType;
import co.com.bcs.tin.xsd.InitiateTransferEntType;
import co.com.bcs.tin.xsd.InstructingAgentType;
import co.com.bcs.tin.xsd.MetadataType;
import co.com.bcs.tin.xsd.OtherType;
import co.com.bcs.tin.xsd.PrivateIdentificationType;
import co.com.bcs.tin.xsd.SettlementInformationType;
import co.com.bcs.tin.xsd.SourceDestinationType;

import co.com.bcs.tin.xsd.TransactionsType;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
/***
import com.minka.IouUtil;
import com.minka.KeyPairHolder;
import com.minka.wallet.Address;
import com.minka.wallet.IOU;
import com.minka.wallet.IouParamsDto;
import com.minka.wallet.MissingRequiredParameterIOUCreation;
import com.minka.wallet.SignatureDto;**/

import common.Transformation;

import java.math.BigDecimal;

import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.DecoderException;

public class TestJson {
    public TestJson() {
        super();
    }

   /** public static void main(String[] arg) {
        try {
            System.out.println("------------INICIO------------------");
            InitiateTransferEntType solicitudVerificacion = getData();
            String json = Transformation.toJson(solicitudVerificacion);
            System.out.println("------------FIN------------------");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static InitiateTransferEntType getData() throws MissingRequiredParameterIOUCreation, DecoderException, NoSuchAlgorithmException{
        InitiateTransferEntType initTrans = new InitiateTransferEntType();
        AmountType amount = new AmountType();

        SourceDestinationType phone = new SourceDestinationType();
        phone.setPhone("571234567890");
        initTrans.setSource(phone);

        phone = new SourceDestinationType();
        phone.setPhone("571987654320");
        initTrans.setDestination(phone);

        amount.setCurrency("COP");
        amount.setValue("260000.00");
        initTrans.setAmount(amount);

        TransactionsType trans = new TransactionsType();
        trans.setAlias("@ALIAS OR ALIAS_HASH");
        IOUType iou = new IOUType();
        iou.setIOUOBJECT(getIOU());
        trans.setIou(iou);
        trans.setTxIdx("INDEX");
        trans.setType("CREDIT");
        initTrans.getTransactions().add(trans);

        MetadataType metadata = new MetadataType();
        metadata.setType("P2P");
        metadata.setStatus("INITIATED");
        metadata.setDescription("Para el Almuerzo");
        metadata.setTransactionsPurpose("TRASLADO");
        metadata.setChannel("APP");
        metadata.setPersonTypeOrigen("N");
        metadata.setPreserveDebtor("TRUE");

        ISO20022Type iso20022Pac00800107 = new ISO20022Type();

        GroupHeaderType groupHeader = new GroupHeaderType();
        groupHeader.setMessageIdentification("ACH.TIN.0000001");
        groupHeader.setCreationDateTime("10-5-2018T10:23:01.123");
        groupHeader.setNumberOfTransaction("1");
        SettlementInformationType settlementInfo = new SettlementInformationType();
        settlementInfo.setSettlementMethod("CLRG");
        ClearingSystemType clSys = new ClearingSystemType();
        clSys.setProprietary("ACH COLOMBIA SA");
        settlementInfo.setClearingSystem(clSys);
        groupHeader.setSettlementInformation(settlementInfo);
        InstructingAgentType instructingAgent = new InstructingAgentType();
        FinInstitutionIdentificationType finInsId = new FinInstitutionIdentificationType();
        finInsId.setBICFI("0051");
        finInsId.setName("Bank_1");
        instructingAgent.setFinantialInstitutionIdentification(finInsId);
        groupHeader.setInstructingAgent(instructingAgent);
        iso20022Pac00800107.setGroupHeader(groupHeader);

        CreditTransferTXInformationType crTransTxInfo = new CreditTransferTXInformationType();
        InfoClientType debtor = new InfoClientType();
        debtor.setName("Luis Fernando Buendia Perez");
        IdentificationType identificationDeb = new IdentificationType();
        PrivateIdentificationType privId = new PrivateIdentificationType();
        OtherType other = new OtherType();
        other.setIdentification("1013018722");
        other.setProprietary("CC");
        privId.setOther(other);
        debtor.setCountryOfResidence("COL");
        ContactDetailsType contactDet = new ContactDetailsType();
        contactDet.setMobileNumber("571234567890");
        contactDet.setEmailAddress("luisfernando@gmail.com");
        debtor.setContactDetails(contactDet);
        identificationDeb.setPrivateIdentification(privId);
        debtor.setIdentification(identificationDeb);
        crTransTxInfo.setDebtor(debtor);

        DebtorAccountType debAcc = new DebtorAccountType();
        Identification idDebAcc = new Identification();
        idDebAcc.setIBAN("ABC123456789");
        debAcc.setIdentification(idDebAcc);
        AccountTypeType debAccType = new AccountTypeType();
        debAccType.setCode("SVGS");
        debAcc.setType(debAccType);
        crTransTxInfo.setDebtorAccount(debAcc);

        DebtorAgentType debAg = new DebtorAgentType();
        finInsId = new FinInstitutionIdentificationType();
        finInsId.setBICFI("0051");
        finInsId.setName("Bank_1");
        debAg.setFinInstIdentification(finInsId);
        crTransTxInfo.setDebtorAgent(debAg);

        iso20022Pac00800107.setCreditTransferTransactionInformation(crTransTxInfo);
        metadata.setIso20022Pac00800107(iso20022Pac00800107);

        DeviceType device = new DeviceType();
        device.setHashFingerPrint("A7149CA46747248D05FE7F7DC2752F6D6BD5396B41DAD7B4F4321277EA557F23F8FE7018C51AC9C8B97954C8370A53171E2C5625892501989F3D074DF4979335");
        device.setIpAddressOrigin("190.242.46.190");
        device.setImeiDispositivoMobile("");
        device.setSimCardCodeMobileDevice("");
        device.setModelMobileDevice("");
        device.setMobileOperator("");
        GeolocationType geoloc = new GeolocationType();
        geoloc.setOcean("");
        geoloc.setLocality("");
        geoloc.setPostalCode("");
        geoloc.setLatitude("");
        geoloc.setLongitude("");
        geoloc.setAdministrativeArea("");
        geoloc.setThoroughfare("");
        geoloc.setCountry("");
        device.setGeolocation(geoloc);
        initTrans.getDevice().add(device);

        initTrans.setMetadata(metadata);

        return initTrans;
    }

    private static IOU getIOU() throws MissingRequiredParameterIOUCreation, DecoderException, NoSuchAlgorithmException {

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.setPrettyPrinting().create();
        KeyPairHolder sourcekeyPairHolder = new KeyPairHolder();
        KeyPairHolder.KeyPairHolderDto keypairDto = sourcekeyPairHolder.getDtoForJson();
        //System.out.println(gson.toJson(keypairDto));
        //System.out.println("Now we generate an address");

        Address address = new Address(keypairDto.getPublico());
        String sourceAddress = address.generate().getValue();
        //System.out.println("addressGenerated : " + sourceAddress);

        IouParamsDto iouParamsDto;
        String domain = "domain";

        String source = "wRLdAFjt8rbHWVzvXkAJB68YPwYRWjvkCj";
        String target = "wVA63LTkhiVfc2fhN7iU6xvmTanp9y9qbU";
        String symbol = source;
        String random = "2eeff2202dc8c7f1db50";

        //System.out.println("source:" + source);
        BigDecimal amount = new BigDecimal(100);
        BigDecimal credit = new BigDecimal(0);
        //System.out.println("We convert the java date to ISO FORMAT");
        String expiry = IouUtil.convertToIsoFormat(new Date());

        iouParamsDto = new IouParamsDto(domain, source, target, amount, credit, symbol, random, null, expiry);

        IouUtil iouUtil = new IouUtil();
        //System.out.println("Here we write the claims");
        IOU theIou = iouUtil.write(iouParamsDto);

        Map<PrivateKey, SignatureDto> signaturePairs = new HashMap<>();
        signaturePairs.put(sourcekeyPairHolder.getSecret(), sourcekeyPairHolder.getBasicSignatureDto(sourceAddress));
        //System.out.println("In order to sign we use the Key,value = PrivateKeys, SignatureDto");
        theIou.sign(signaturePairs);

        //System.out.println("Printing pretty JSON ");
        //System.out.println(theIou.toPrettyJson());
        //System.out.println("Printing Raw JSON ");
        //System.out.println(theIou.toRawJson());

        //System.out.println("Printing pretty JSON for the FORMAT V022");
        //System.out.println(theIou.toPrettyJsonV022());
        System.out.println("Printing raw JSON for the FORMAT V022");
        System.out.println(theIou.toRawJsonV022());

        return theIou;
    }**/
}
