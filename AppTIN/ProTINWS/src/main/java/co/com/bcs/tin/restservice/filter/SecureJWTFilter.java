/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.bcs.tin.restservice.filter;

import com.auth0.jwt.interfaces.DecodedJWT;
import javax.annotation.Priority;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

import org.pmw.tinylog.Logger;
import util.MainJWT;

@Provider
@Secured
@Priority(Priorities.AUTHENTICATION)
public class SecureJWTFilter implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {

        // Get the HTTP Authorization header from the request
        final String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
        final String secret = requestContext.getHeaderString("secret");
        Logger.info("#### authorizationHeader : " + authorizationHeader);

        // Check if the HTTP Authorization header is present and formatted correctly
        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
            Logger.error("#### invalid authorizationHeader ");
            throw new NotAuthorizedException("Authorization header must be provided");
        }

        // Extract the token from the HTTP Authorization header
        String token = authorizationHeader.substring("Bearer".length()).trim();

        try {
            DecodedJWT jwt = MainJWT.verifyToken(token, secret);
            if (jwt == null) {
                Logger.error("####Token invalido ");
                requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
            }
            Logger.info("#### token valido : " + token);

        } catch (Exception e) {
            Logger.error("#### token invalido : " + token);
            requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
        }
    }
}
