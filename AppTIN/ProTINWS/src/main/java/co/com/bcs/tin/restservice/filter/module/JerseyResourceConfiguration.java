/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.bcs.tin.restservice.filter.module;

import javax.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;

/**
 *
 * @author jsanchez
 */
@ApplicationPath("services")
public class JerseyResourceConfiguration extends ResourceConfig {
    public JerseyResourceConfiguration() {
        packages("co.com.bcs.tin"); 

    }
}
