package co.com.bcs.tin.service;

import co.com.bcs.tin.xsd.AcceptTransferEntType;
import co.com.bcs.tin.xsd.AcceptTransferSalType;
import co.com.bcs.tin.xsd.InitiateTransferEntType;
import co.com.bcs.tin.xsd.InitiateTransferSalType;
import co.com.bcs.tin.xsd.ListTransferEntType;
import co.com.bcs.tin.xsd.ListTransferSalType;
import co.com.bcs.tin.xsd.ObjectFactory;
import co.com.bcs.tin.xsd.RejectTransferEntType;
import co.com.bcs.tin.xsd.RejectTransferSalType;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import javax.xml.bind.annotation.XmlSeeAlso;

@WebService(name = "TINPort", targetNamespace = "http://service.tin.bcs.com.co", serviceName = "TINService",
            portName = "TINServicePort", wsdlLocation = "/WEB-INF/bcs_tin/TinService.wsdl")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({ ObjectFactory.class })
public class TINPortImpl {
    public TINPortImpl() {
    }

    @WebResult(name = "InitiateTransferSal", partName = "parameters", targetNamespace = "http://xsd.tin.bcs.com.co")
    @WebMethod(action = "http://service.tin.bcs.com.co/initiateTransfer")
    public InitiateTransferSalType initiateTransfer(@WebParam(name = "InitiateTransferEnt", partName = "parameters",
                                                              targetNamespace = "http://xsd.tin.bcs.com.co")
                                                    InitiateTransferEntType parameters) {
        return null;
    }

    @WebResult(name = "ListTransferSal", partName = "parameters", targetNamespace = "http://xsd.tin.bcs.com.co")
    @WebMethod(action = "http://service.tin.bcs.com.co/listTransfer")
    public ListTransferSalType listTransfer(@WebParam(name = "ListTransferEnt", partName = "parameters",
                                                      targetNamespace = "http://xsd.tin.bcs.com.co")
                                            ListTransferEntType parameters) {
        return null;
    }

    @WebResult(name = "AcceptTransferSal", partName = "parameters", targetNamespace = "http://xsd.tin.bcs.com.co")
    @WebMethod(action = "http://service.tin.bcs.com.co/acceptTransfer")
    public AcceptTransferSalType acceptTransfer(@WebParam(name = "AcceptTransferEnt", partName = "parameters",
                                                          targetNamespace = "http://xsd.tin.bcs.com.co")
                                                AcceptTransferEntType parameters) {
        return null;
    }

    @WebResult(name = "RejectTransferSal", partName = "parameters", targetNamespace = "http://xsd.tin.bcs.com.co")
    @WebMethod(action = "http://service.tin.bcs.com.co/rejectTransfer")
    public RejectTransferSalType rejectTransfer(@WebParam(name = "RejectTransferEnt", partName = "parameters",
                                                          targetNamespace = "http://xsd.tin.bcs.com.co")
                                                RejectTransferEntType parameters) {
        return null;
    }
}
