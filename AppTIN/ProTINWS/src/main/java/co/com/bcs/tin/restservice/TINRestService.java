package co.com.bcs.tin.restservice;

import co.com.bcs.tin.json.DebitCreditTransferInType;

import co.com.bcs.tin.json.DebitCreditTransferOutType;

import co.com.bcs.tin.xsd.AmountType;
import co.com.bcs.tin.xsd.ClearingSystemType;
import co.com.bcs.tin.xsd.FinInstitutionIdentificationType;
import co.com.bcs.tin.xsd.GroupHeaderType;
import co.com.bcs.tin.xsd.IOUType;
import co.com.bcs.tin.xsd.ISO20022Type;
import co.com.bcs.tin.xsd.InstructingAgentType;
import co.com.bcs.tin.xsd.MetadataType;
import co.com.bcs.tin.xsd.SettlementInformationType;
import co.com.bcs.tin.xsd.SourceDestinationType;
import co.com.bcs.tin.xsd.TransactionsType;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import co.com.bcs.tin.restservice.filter.Secured;
import com.auth0.jwt.exceptions.JWTVerificationException;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import util.MainJWT;

@Path("/v1")
//@Consumes({"application/json"})
public class TINRestService {

    public TINRestService() {
        super();
    }

    @POST
    @Path("/authenticate")
    @Produces(MediaType.APPLICATION_JSON)
    public Response authenticate(@DefaultValue("secret") @QueryParam("secret") String secret) {
        //TODO se debe eliminar valor por default a blancos.
        try {
            String token = MainJWT.createToken(secret);
            return Response.status(201).entity(token).build(); 
        } catch (JWTVerificationException exception) {
            exception.printStackTrace();
            return Response.status(401).build();
        }
    }

    @POST
    @Path("/accept")
    @Secured
    public DebitCreditTransferOutType aceptarDebito(DebitCreditTransferInType debitTransferIn) {
        System.out.println("Prueba aceptarDebito");
        return null;
    }

    @POST
    @Path("/reject")
    public DebitCreditTransferOutType rechazarDebito(DebitCreditTransferInType debitTransferIn) {
        return null;
    }

    @POST
    @Path("/credit")
    public DebitCreditTransferOutType credito(DebitCreditTransferInType creditTransferIn) {
        return null;
    }

    public static DebitCreditTransferInType getData() {
        DebitCreditTransferInType infoTransfer = new DebitCreditTransferInType();
        AmountType amount = new AmountType();

        SourceDestinationType phone = new SourceDestinationType();
        phone.setPhone("571234567890");
        infoTransfer.setSource(phone);

        phone = new SourceDestinationType();
        phone.setPhone("571987654320");
        infoTransfer.setDestination(phone);

        amount.setCurrency("COP");
        amount.setValue("260000.00");
        infoTransfer.setAmount(amount);

        TransactionsType trans = new TransactionsType();
        trans.setAlias("@ALIAS OR ALIAS_HASH");
        IOUType iou = new IOUType();
        iou.setIOUOBJECT("getIOU()");
        trans.setIou(iou);
        trans.setTxIdx("INDEX");
        trans.setType("CREDIT");
        infoTransfer.getTransactions().add(trans);

        MetadataType metadata = new MetadataType();
        metadata.setType("P2P");
        metadata.setStatus("INITIATED");
        metadata.setDescription("Para el Almuerzo");
        metadata.setTransactionsPurpose("TRASLADO");
        metadata.setChannel("APP");
        metadata.setPersonTypeOrigen("N");
        metadata.setPreserveDebtor("TRUE");

        ISO20022Type iso20022Pac002001009 = new ISO20022Type();

        GroupHeaderType groupHeader = new GroupHeaderType();
        groupHeader.setMessageIdentification("ACH.TIN.0000001");
        groupHeader.setCreationDateTime("10-5-2018T10:23:01.123");

        SettlementInformationType settlementInfo = new SettlementInformationType();
        settlementInfo.setSettlementMethod("CLRG");
        ClearingSystemType clSys = new ClearingSystemType();
        clSys.setProprietary("ACH COLOMBIA SA");
        settlementInfo.setClearingSystem(clSys);
        groupHeader.setSettlementInformation(settlementInfo);

        InstructingAgentType instructingAgent = new InstructingAgentType();
        FinInstitutionIdentificationType finInsId = new FinInstitutionIdentificationType();
        finInsId.setBICFI("0051");
        finInsId.setName("Bank_1");
        instructingAgent.setFinantialInstitutionIdentification(finInsId);
        groupHeader.setInstructingAgent(instructingAgent);
        iso20022Pac002001009.setGroupHeader(groupHeader);

        metadata.setIso20022Pac002001009(iso20022Pac002001009);

        infoTransfer.setMetadata(metadata);

        return infoTransfer;
    }
}
