
package co.com.bcs.tin.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RejectTransferSalType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="RejectTransferSalType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="respuestaconfirmarRechazarTransf" type="{http://xsd.tin.bcs.com.co}RepuestaConfRechaTransferenciaType"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RejectTransferSalType", propOrder = {


    })
public class RejectTransferSalType {

    @XmlElement(required = true)
    protected RepuestaConfRechaTransferenciaType respuestaconfirmarRechazarTransf;

    /**
     * Gets the value of the respuestaconfirmarRechazarTransf property.
     *
     * @return
     *     possible object is
     *     {@link RepuestaConfRechaTransferenciaType }
     *
     */
    public RepuestaConfRechaTransferenciaType getRespuestaconfirmarRechazarTransf() {
        return respuestaconfirmarRechazarTransf;
    }

    /**
     * Sets the value of the respuestaconfirmarRechazarTransf property.
     *
     * @param value
     *     allowed object is
     *     {@link RepuestaConfRechaTransferenciaType }
     *
     */
    public void setRespuestaconfirmarRechazarTransf(RepuestaConfRechaTransferenciaType value) {
        this.respuestaconfirmarRechazarTransf = value;
    }

}
