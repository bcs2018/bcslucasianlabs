
package co.com.bcs.tin.xsd;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ConfirmarRechazarTransferenciaType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ConfirmarRechazarTransferenciaType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="source" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="destination" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="amount" type="{http://xsd.tin.bcs.com.co}AmountType"/&gt;
 *         &lt;element name="transactions" type="{http://xsd.tin.bcs.com.co}TransactionsType"/&gt;
 *         &lt;element name="metadata" type="{http://xsd.tin.bcs.com.co}MetadataType"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConfirmarRechazarTransferenciaType", propOrder = {


    })
public class ConfirmarRechazarTransferenciaType {

    @XmlElement(required = true)
    protected BigDecimal source;
    @XmlElement(required = true)
    protected BigDecimal destination;
    @XmlElement(required = true)
    protected AmountType amount;
    @XmlElement(required = true)
    protected TransactionsType transactions;
    @XmlElement(required = true)
    protected MetadataType metadata;

    /**
     * Gets the value of the source property.
     *
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *
     */
    public BigDecimal getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     *
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *
     */
    public void setSource(BigDecimal value) {
        this.source = value;
    }

    /**
     * Gets the value of the destination property.
     *
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *
     */
    public BigDecimal getDestination() {
        return destination;
    }

    /**
     * Sets the value of the destination property.
     *
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *
     */
    public void setDestination(BigDecimal value) {
        this.destination = value;
    }

    /**
     * Gets the value of the amount property.
     *
     * @return
     *     possible object is
     *     {@link AmountType }
     *
     */
    public AmountType getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     *
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *
     */
    public void setAmount(AmountType value) {
        this.amount = value;
    }

    /**
     * Gets the value of the transactions property.
     *
     * @return
     *     possible object is
     *     {@link TransactionsType }
     *
     */
    public TransactionsType getTransactions() {
        return transactions;
    }

    /**
     * Sets the value of the transactions property.
     *
     * @param value
     *     allowed object is
     *     {@link TransactionsType }
     *
     */
    public void setTransactions(TransactionsType value) {
        this.transactions = value;
    }

    /**
     * Gets the value of the metadata property.
     *
     * @return
     *     possible object is
     *     {@link MetadataType }
     *
     */
    public MetadataType getMetadata() {
        return metadata;
    }

    /**
     * Sets the value of the metadata property.
     *
     * @param value
     *     allowed object is
     *     {@link MetadataType }
     *
     */
    public void setMetadata(MetadataType value) {
        this.metadata = value;
    }

}
