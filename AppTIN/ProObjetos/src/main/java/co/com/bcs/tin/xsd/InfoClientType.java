
package co.com.bcs.tin.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InfoClientType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="InfoClientType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="identification" type="{http://xsd.tin.bcs.com.co}identificationType"/&gt;
 *         &lt;element name="countryOfResidence" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="contactDetails" type="{http://xsd.tin.bcs.com.co}ContactDetailsType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InfoClientType", propOrder = { "name", "identification", "countryOfResidence", "contactDetails" })
public class InfoClientType {

    @XmlElement(required = true)
    protected String name;
    @XmlElement(required = true)
    protected IdentificationType identification;
    @XmlElement(required = true)
    protected String countryOfResidence;
    @XmlElement(required = true)
    protected ContactDetailsType contactDetails;

    /**
     * Gets the value of the name property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the identification property.
     *
     * @return
     *     possible object is
     *     {@link IdentificationType }
     *
     */
    public IdentificationType getIdentification() {
        return identification;
    }

    /**
     * Sets the value of the identification property.
     *
     * @param value
     *     allowed object is
     *     {@link IdentificationType }
     *
     */
    public void setIdentification(IdentificationType value) {
        this.identification = value;
    }

    /**
     * Gets the value of the countryOfResidence property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCountryOfResidence() {
        return countryOfResidence;
    }

    /**
     * Sets the value of the countryOfResidence property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCountryOfResidence(String value) {
        this.countryOfResidence = value;
    }

    /**
     * Gets the value of the contactDetails property.
     *
     * @return
     *     possible object is
     *     {@link ContactDetailsType }
     *
     */
    public ContactDetailsType getContactDetails() {
        return contactDetails;
    }

    /**
     * Sets the value of the contactDetails property.
     *
     * @param value
     *     allowed object is
     *     {@link ContactDetailsType }
     *
     */
    public void setContactDetails(ContactDetailsType value) {
        this.contactDetails = value;
    }

}
