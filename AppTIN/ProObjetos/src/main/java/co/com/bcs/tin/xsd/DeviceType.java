
package co.com.bcs.tin.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DeviceType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="DeviceType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="hashFingerPrint" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ipAddressOrigin" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="imeiDispositivoMobile" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="simCardCodeMobileDevice" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="modelMobileDevice" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="mobileOperator" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="geolocation" type="{http://xsd.tin.bcs.com.co}GeolocationType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeviceType",
         propOrder =
         { "hashFingerPrint", "ipAddressOrigin", "imeiDispositivoMobile", "simCardCodeMobileDevice",
           "modelMobileDevice", "mobileOperator", "geolocation"
    })
public class DeviceType {

    @XmlElement(required = true)
    protected String hashFingerPrint;
    @XmlElement(required = true)
    protected String ipAddressOrigin;
    @XmlElement(required = true)
    protected String imeiDispositivoMobile;
    @XmlElement(required = true)
    protected String simCardCodeMobileDevice;
    @XmlElement(required = true)
    protected String modelMobileDevice;
    @XmlElement(required = true)
    protected String mobileOperator;
    @XmlElement(required = true)
    protected GeolocationType geolocation;

    /**
     * Gets the value of the hashFingerPrint property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getHashFingerPrint() {
        return hashFingerPrint;
    }

    /**
     * Sets the value of the hashFingerPrint property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setHashFingerPrint(String value) {
        this.hashFingerPrint = value;
    }

    /**
     * Gets the value of the ipAddressOrigin property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getIpAddressOrigin() {
        return ipAddressOrigin;
    }

    /**
     * Sets the value of the ipAddressOrigin property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setIpAddressOrigin(String value) {
        this.ipAddressOrigin = value;
    }

    /**
     * Gets the value of the imeiDispositivoMobile property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getImeiDispositivoMobile() {
        return imeiDispositivoMobile;
    }

    /**
     * Sets the value of the imeiDispositivoMobile property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setImeiDispositivoMobile(String value) {
        this.imeiDispositivoMobile = value;
    }

    /**
     * Gets the value of the simCardCodeMobileDevice property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSimCardCodeMobileDevice() {
        return simCardCodeMobileDevice;
    }

    /**
     * Sets the value of the simCardCodeMobileDevice property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSimCardCodeMobileDevice(String value) {
        this.simCardCodeMobileDevice = value;
    }

    /**
     * Gets the value of the modelMobileDevice property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getModelMobileDevice() {
        return modelMobileDevice;
    }

    /**
     * Sets the value of the modelMobileDevice property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setModelMobileDevice(String value) {
        this.modelMobileDevice = value;
    }

    /**
     * Gets the value of the mobileOperator property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMobileOperator() {
        return mobileOperator;
    }

    /**
     * Sets the value of the mobileOperator property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMobileOperator(String value) {
        this.mobileOperator = value;
    }

    /**
     * Gets the value of the geolocation property.
     *
     * @return
     *     possible object is
     *     {@link GeolocationType }
     *
     */
    public GeolocationType getGeolocation() {
        return geolocation;
    }

    /**
     * Sets the value of the geolocation property.
     *
     * @param value
     *     allowed object is
     *     {@link GeolocationType }
     *
     */
    public void setGeolocation(GeolocationType value) {
        this.geolocation = value;
    }

}
