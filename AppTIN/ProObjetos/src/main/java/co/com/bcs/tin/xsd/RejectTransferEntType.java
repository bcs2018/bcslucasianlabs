
package co.com.bcs.tin.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RejectTransferEntType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="RejectTransferEntType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="confirmarRechazarTransferencia" type="{http://xsd.tin.bcs.com.co}ConfirmarRechazarTransferenciaType"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RejectTransferEntType", propOrder = {


    })
public class RejectTransferEntType {

    @XmlElement(required = true)
    protected ConfirmarRechazarTransferenciaType confirmarRechazarTransferencia;

    /**
     * Gets the value of the confirmarRechazarTransferencia property.
     *
     * @return
     *     possible object is
     *     {@link ConfirmarRechazarTransferenciaType }
     *
     */
    public ConfirmarRechazarTransferenciaType getConfirmarRechazarTransferencia() {
        return confirmarRechazarTransferencia;
    }

    /**
     * Sets the value of the confirmarRechazarTransferencia property.
     *
     * @param value
     *     allowed object is
     *     {@link ConfirmarRechazarTransferenciaType }
     *
     */
    public void setConfirmarRechazarTransferencia(ConfirmarRechazarTransferenciaType value) {
        this.confirmarRechazarTransferencia = value;
    }

}
