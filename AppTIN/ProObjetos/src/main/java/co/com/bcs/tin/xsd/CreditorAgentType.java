
package co.com.bcs.tin.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreditorAgentType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CreditorAgentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="finInstIdentification" type="{http://xsd.tin.bcs.com.co}FinInstitutionIdentificationType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditorAgentType", propOrder = { "finInstIdentification" })
public class CreditorAgentType {

    @XmlElement(required = true)
    protected FinInstitutionIdentificationType finInstIdentification;

    /**
     * Gets the value of the finInstIdentification property.
     *
     * @return
     *     possible object is
     *     {@link FinInstitutionIdentificationType }
     *
     */
    public FinInstitutionIdentificationType getFinInstIdentification() {
        return finInstIdentification;
    }

    /**
     * Sets the value of the finInstIdentification property.
     *
     * @param value
     *     allowed object is
     *     {@link FinInstitutionIdentificationType }
     *
     */
    public void setFinInstIdentification(FinInstitutionIdentificationType value) {
        this.finInstIdentification = value;
    }

}
