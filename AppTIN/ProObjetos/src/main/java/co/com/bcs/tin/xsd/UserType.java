
package co.com.bcs.tin.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for userType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="userType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="phone" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="address" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="user_identification" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="bank_code" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="user_account" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "userType", propOrder = {


    })
public class UserType {

    @XmlElement(required = true)
    protected String phone;
    @XmlElement(required = true)
    protected String address;
    @XmlElement(required = true)
    protected String name;
    @XmlElement(name = "user_identification", required = true)
    protected String userIdentification;
    @XmlElement(name = "bank_code", required = true)
    protected String bankCode;
    @XmlElement(name = "user_account", required = true)
    protected String userAccount;

    /**
     * Gets the value of the phone property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Sets the value of the phone property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPhone(String value) {
        this.phone = value;
    }

    /**
     * Gets the value of the address property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets the value of the address property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAddress(String value) {
        this.address = value;
    }

    /**
     * Gets the value of the name property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the userIdentification property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getUserIdentification() {
        return userIdentification;
    }

    /**
     * Sets the value of the userIdentification property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setUserIdentification(String value) {
        this.userIdentification = value;
    }

    /**
     * Gets the value of the bankCode property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getBankCode() {
        return bankCode;
    }

    /**
     * Sets the value of the bankCode property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setBankCode(String value) {
        this.bankCode = value;
    }

    /**
     * Gets the value of the userAccount property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getUserAccount() {
        return userAccount;
    }

    /**
     * Sets the value of the userAccount property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setUserAccount(String value) {
        this.userAccount = value;
    }

}
