
package co.com.bcs.tin.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the co.com.bcs.tin.xsd package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 *
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _InitiateTransferEnt_QNAME =
        new QName("http://xsd.tin.bcs.com.co", "InitiateTransferEnt");
    private final static QName _InitiateTransferSal_QNAME =
        new QName("http://xsd.tin.bcs.com.co", "InitiateTransferSal");
    private final static QName _ListTransferEnt_QNAME = new QName("http://xsd.tin.bcs.com.co", "ListTransferEnt");
    private final static QName _ListTransferSal_QNAME = new QName("http://xsd.tin.bcs.com.co", "ListTransferSal");
    private final static QName _AcceptTransferEnt_QNAME = new QName("http://xsd.tin.bcs.com.co", "AcceptTransferEnt");
    private final static QName _AcceptTransferSal_QNAME = new QName("http://xsd.tin.bcs.com.co", "AcceptTransferSal");
    private final static QName _RejectTransferEnt_QNAME = new QName("http://xsd.tin.bcs.com.co", "RejectTransferEnt");
    private final static QName _RejectTransferSal_QNAME = new QName("http://xsd.tin.bcs.com.co", "RejectTransferSal");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.com.bcs.tin.xsd
     *
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link InitiateTransferEntType }
     *
     */
    public InitiateTransferEntType createInitiateTransferEntType() {
        return new InitiateTransferEntType();
    }

    /**
     * Create an instance of {@link InitiateTransferSalType }
     *
     */
    public InitiateTransferSalType createInitiateTransferSalType() {
        return new InitiateTransferSalType();
    }

    /**
     * Create an instance of {@link ListTransferEntType }
     *
     */
    public ListTransferEntType createListTransferEntType() {
        return new ListTransferEntType();
    }

    /**
     * Create an instance of {@link ListTransferSalType }
     *
     */
    public ListTransferSalType createListTransferSalType() {
        return new ListTransferSalType();
    }

    /**
     * Create an instance of {@link AcceptTransferEntType }
     *
     */
    public AcceptTransferEntType createAcceptTransferEntType() {
        return new AcceptTransferEntType();
    }

    /**
     * Create an instance of {@link AcceptTransferSalType }
     *
     */
    public AcceptTransferSalType createAcceptTransferSalType() {
        return new AcceptTransferSalType();
    }

    /**
     * Create an instance of {@link RejectTransferEntType }
     *
     */
    public RejectTransferEntType createRejectTransferEntType() {
        return new RejectTransferEntType();
    }

    /**
     * Create an instance of {@link RejectTransferSalType }
     *
     */
    public RejectTransferSalType createRejectTransferSalType() {
        return new RejectTransferSalType();
    }

    /**
     * Create an instance of {@link ListTransferArrayType }
     *
     */
    public ListTransferArrayType createListTransferArrayType() {
        return new ListTransferArrayType();
    }

    /**
     * Create an instance of {@link AmountType }
     *
     */
    public AmountType createAmountType() {
        return new AmountType();
    }

    /**
     * Create an instance of {@link TransactionsType }
     *
     */
    public TransactionsType createTransactionsType() {
        return new TransactionsType();
    }

    /**
     * Create an instance of {@link IOUType }
     *
     */
    public IOUType createIOUType() {
        return new IOUType();
    }

    /**
     * Create an instance of {@link MetadataType }
     *
     */
    public MetadataType createMetadataType() {
        return new MetadataType();
    }

    /**
     * Create an instance of {@link ISO20022Type }
     *
     */
    public ISO20022Type createISO20022Type() {
        return new ISO20022Type();
    }

    /**
     * Create an instance of {@link GroupHeaderType }
     *
     */
    public GroupHeaderType createGroupHeaderType() {
        return new GroupHeaderType();
    }

    /**
     * Create an instance of {@link SettlementInformationType }
     *
     */
    public SettlementInformationType createSettlementInformationType() {
        return new SettlementInformationType();
    }

    /**
     * Create an instance of {@link ClearingSystemType }
     *
     */
    public ClearingSystemType createClearingSystemType() {
        return new ClearingSystemType();
    }

    /**
     * Create an instance of {@link InstructingAgentType }
     *
     */
    public InstructingAgentType createInstructingAgentType() {
        return new InstructingAgentType();
    }

    /**
     * Create an instance of {@link FinInstitutionIdentificationType }
     *
     */
    public FinInstitutionIdentificationType createFinInstitutionIdentificationType() {
        return new FinInstitutionIdentificationType();
    }

    /**
     * Create an instance of {@link CreditTransferTXInformationType }
     *
     */
    public CreditTransferTXInformationType createCreditTransferTXInformationType() {
        return new CreditTransferTXInformationType();
    }

    /**
     * Create an instance of {@link IdentificationType }
     *
     */
    public IdentificationType createIdentificationType() {
        return new IdentificationType();
    }

    /**
     * Create an instance of {@link PrivateIdentificationType }
     *
     */
    public PrivateIdentificationType createPrivateIdentificationType() {
        return new PrivateIdentificationType();
    }

    /**
     * Create an instance of {@link AccountTypeType }
     *
     */
    public AccountTypeType createAccountTypeType() {
        return new AccountTypeType();
    }

    /**
     * Create an instance of {@link AntiFraudAnalysisType }
     *
     */
    public AntiFraudAnalysisType createAntiFraudAnalysisType() {
        return new AntiFraudAnalysisType();
    }

    /**
     * Create an instance of {@link ConfirmarRechazarTransferenciaType }
     *
     */
    public ConfirmarRechazarTransferenciaType createConfirmarRechazarTransferenciaType() {
        return new ConfirmarRechazarTransferenciaType();
    }

    /**
     * Create an instance of {@link CreditorAgentType }
     *
     */
    public CreditorAgentType createCreditorAgentType() {
        return new CreditorAgentType();
    }

    /**
     * Create an instance of {@link DebtorAccountType }
     *
     */
    public DebtorAccountType createDebtorAccountType() {
        return new DebtorAccountType();
    }

    /**
     * Create an instance of {@link DebtorAgentType }
     *
     */
    public DebtorAgentType createDebtorAgentType() {
        return new DebtorAgentType();
    }

    /**
     * Create an instance of {@link ContactDetailsType }
     *
     */
    public ContactDetailsType createContactDetailsType() {
        return new ContactDetailsType();
    }

    /**
     * Create an instance of {@link CreditorAccountType }
     *
     */
    public CreditorAccountType createCreditorAccountType() {
        return new CreditorAccountType();
    }

    /**
     * Create an instance of {@link DeviceType }
     *
     */
    public DeviceType createDeviceType() {
        return new DeviceType();
    }

    /**
     * Create an instance of {@link GeolocationType }
     *
     */
    public GeolocationType createGeolocationType() {
        return new GeolocationType();
    }

    /**
     * Create an instance of {@link Identification }
     *
     */
    public Identification createIdentification() {
        return new Identification();
    }

    /**
     * Create an instance of {@link InfoClientType }
     *
     */
    public InfoClientType createInfoClientType() {
        return new InfoClientType();
    }

    /**
     * Create an instance of {@link OriginalGroupInfoAndStatusType }
     *
     */
    public OriginalGroupInfoAndStatusType createOriginalGroupInfoAndStatusType() {
        return new OriginalGroupInfoAndStatusType();
    }

    /**
     * Create an instance of {@link OtherType }
     *
     */
    public OtherType createOtherType() {
        return new OtherType();
    }

    /**
     * Create an instance of {@link RepuestaConfRechaTransferenciaType }
     *
     */
    public RepuestaConfRechaTransferenciaType createRepuestaConfRechaTransferenciaType() {
        return new RepuestaConfRechaTransferenciaType();
    }

    /**
     * Create an instance of {@link SourceDestinationType }
     *
     */
    public SourceDestinationType createSourceDestinationType() {
        return new SourceDestinationType();
    }

    /**
     * Create an instance of {@link StatusReasonInformationType }
     *
     */
    public StatusReasonInformationType createStatusReasonInformationType() {
        return new StatusReasonInformationType();
    }

    /**
     * Create an instance of {@link TrustLinkType }
     *
     */
    public TrustLinkType createTrustLinkType() {
        return new TrustLinkType();
    }

    /**
     * Create an instance of {@link UserType }
     *
     */
    public UserType createUserType() {
        return new UserType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InitiateTransferEntType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://xsd.tin.bcs.com.co", name = "InitiateTransferEnt")
    public JAXBElement<InitiateTransferEntType> createInitiateTransferEnt(InitiateTransferEntType value) {
        return new JAXBElement<InitiateTransferEntType>(_InitiateTransferEnt_QNAME, InitiateTransferEntType.class, null,
                                                        value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InitiateTransferSalType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://xsd.tin.bcs.com.co", name = "InitiateTransferSal")
    public JAXBElement<InitiateTransferSalType> createInitiateTransferSal(InitiateTransferSalType value) {
        return new JAXBElement<InitiateTransferSalType>(_InitiateTransferSal_QNAME, InitiateTransferSalType.class, null,
                                                        value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListTransferEntType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://xsd.tin.bcs.com.co", name = "ListTransferEnt")
    public JAXBElement<ListTransferEntType> createListTransferEnt(ListTransferEntType value) {
        return new JAXBElement<ListTransferEntType>(_ListTransferEnt_QNAME, ListTransferEntType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListTransferSalType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://xsd.tin.bcs.com.co", name = "ListTransferSal")
    public JAXBElement<ListTransferSalType> createListTransferSal(ListTransferSalType value) {
        return new JAXBElement<ListTransferSalType>(_ListTransferSal_QNAME, ListTransferSalType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AcceptTransferEntType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://xsd.tin.bcs.com.co", name = "AcceptTransferEnt")
    public JAXBElement<AcceptTransferEntType> createAcceptTransferEnt(AcceptTransferEntType value) {
        return new JAXBElement<AcceptTransferEntType>(_AcceptTransferEnt_QNAME, AcceptTransferEntType.class, null,
                                                      value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AcceptTransferSalType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://xsd.tin.bcs.com.co", name = "AcceptTransferSal")
    public JAXBElement<AcceptTransferSalType> createAcceptTransferSal(AcceptTransferSalType value) {
        return new JAXBElement<AcceptTransferSalType>(_AcceptTransferSal_QNAME, AcceptTransferSalType.class, null,
                                                      value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RejectTransferEntType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://xsd.tin.bcs.com.co", name = "RejectTransferEnt")
    public JAXBElement<RejectTransferEntType> createRejectTransferEnt(RejectTransferEntType value) {
        return new JAXBElement<RejectTransferEntType>(_RejectTransferEnt_QNAME, RejectTransferEntType.class, null,
                                                      value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RejectTransferSalType }{@code >}}
     *
     */
    @XmlElementDecl(namespace = "http://xsd.tin.bcs.com.co", name = "RejectTransferSal")
    public JAXBElement<RejectTransferSalType> createRejectTransferSal(RejectTransferSalType value) {
        return new JAXBElement<RejectTransferSalType>(_RejectTransferSal_QNAME, RejectTransferSalType.class, null,
                                                      value);
    }

}
