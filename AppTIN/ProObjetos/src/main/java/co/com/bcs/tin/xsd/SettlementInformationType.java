
package co.com.bcs.tin.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SettlementInformationType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="SettlementInformationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="settlement_method" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="clearing_system" type="{http://xsd.tin.bcs.com.co}ClearingSystemType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SettlementInformationType", propOrder = { "settlementMethod", "clearingSystem" })
public class SettlementInformationType {

    @XmlElement(name = "settlement_method", required = true)
    protected String settlementMethod;
    @XmlElement(name = "clearing_system", required = true)
    protected ClearingSystemType clearingSystem;

    /**
     * Gets the value of the settlementMethod property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSettlementMethod() {
        return settlementMethod;
    }

    /**
     * Sets the value of the settlementMethod property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSettlementMethod(String value) {
        this.settlementMethod = value;
    }

    /**
     * Gets the value of the clearingSystem property.
     *
     * @return
     *     possible object is
     *     {@link ClearingSystemType }
     *
     */
    public ClearingSystemType getClearingSystem() {
        return clearingSystem;
    }

    /**
     * Sets the value of the clearingSystem property.
     *
     * @param value
     *     allowed object is
     *     {@link ClearingSystemType }
     *
     */
    public void setClearingSystem(ClearingSystemType value) {
        this.clearingSystem = value;
    }

}
