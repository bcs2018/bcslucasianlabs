
package co.com.bcs.tin.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GroupHeaderType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="GroupHeaderType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="message_identification" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="creation_date_time" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="number_of_transaction" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="settlement_information" type="{http://xsd.tin.bcs.com.co}SettlementInformationType"/&gt;
 *         &lt;element name="instructing_agent" type="{http://xsd.tin.bcs.com.co}InstructingAgentType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GroupHeaderType",
         propOrder =
         { "messageIdentification", "creationDateTime", "numberOfTransaction", "settlementInformation",
           "instructingAgent"
    })
public class GroupHeaderType {

    @XmlElement(name = "message_identification", required = true)
    protected String messageIdentification;
    @XmlElement(name = "creation_date_time", required = true)
    protected String creationDateTime;
    @XmlElement(name = "number_of_transaction", required = true)
    protected String numberOfTransaction;
    @XmlElement(name = "settlement_information", required = true)
    protected SettlementInformationType settlementInformation;
    @XmlElement(name = "instructing_agent", required = true)
    protected InstructingAgentType instructingAgent;

    /**
     * Gets the value of the messageIdentification property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMessageIdentification() {
        return messageIdentification;
    }

    /**
     * Sets the value of the messageIdentification property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMessageIdentification(String value) {
        this.messageIdentification = value;
    }

    /**
     * Gets the value of the creationDateTime property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCreationDateTime() {
        return creationDateTime;
    }

    /**
     * Sets the value of the creationDateTime property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCreationDateTime(String value) {
        this.creationDateTime = value;
    }

    /**
     * Gets the value of the numberOfTransaction property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getNumberOfTransaction() {
        return numberOfTransaction;
    }

    /**
     * Sets the value of the numberOfTransaction property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setNumberOfTransaction(String value) {
        this.numberOfTransaction = value;
    }

    /**
     * Gets the value of the settlementInformation property.
     *
     * @return
     *     possible object is
     *     {@link SettlementInformationType }
     *
     */
    public SettlementInformationType getSettlementInformation() {
        return settlementInformation;
    }

    /**
     * Sets the value of the settlementInformation property.
     *
     * @param value
     *     allowed object is
     *     {@link SettlementInformationType }
     *
     */
    public void setSettlementInformation(SettlementInformationType value) {
        this.settlementInformation = value;
    }

    /**
     * Gets the value of the instructingAgent property.
     *
     * @return
     *     possible object is
     *     {@link InstructingAgentType }
     *
     */
    public InstructingAgentType getInstructingAgent() {
        return instructingAgent;
    }

    /**
     * Sets the value of the instructingAgent property.
     *
     * @param value
     *     allowed object is
     *     {@link InstructingAgentType }
     *
     */
    public void setInstructingAgent(InstructingAgentType value) {
        this.instructingAgent = value;
    }

}
