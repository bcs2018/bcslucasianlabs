
package co.com.bcs.tin.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreditorAccountType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CreditorAccountType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="identification" type="{http://xsd.tin.bcs.com.co}Identification"/&gt;
 *         &lt;element name="type" type="{http://xsd.tin.bcs.com.co}AccountTypeType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditorAccountType", propOrder = { "identification", "type" })
public class CreditorAccountType {

    @XmlElement(required = true)
    protected Identification identification;
    @XmlElement(required = true)
    protected AccountTypeType type;

    /**
     * Gets the value of the identification property.
     *
     * @return
     *     possible object is
     *     {@link Identification }
     *
     */
    public Identification getIdentification() {
        return identification;
    }

    /**
     * Sets the value of the identification property.
     *
     * @param value
     *     allowed object is
     *     {@link Identification }
     *
     */
    public void setIdentification(Identification value) {
        this.identification = value;
    }

    /**
     * Gets the value of the type property.
     *
     * @return
     *     possible object is
     *     {@link AccountTypeType }
     *
     */
    public AccountTypeType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     *
     * @param value
     *     allowed object is
     *     {@link AccountTypeType }
     *
     */
    public void setType(AccountTypeType value) {
        this.type = value;
    }

}
