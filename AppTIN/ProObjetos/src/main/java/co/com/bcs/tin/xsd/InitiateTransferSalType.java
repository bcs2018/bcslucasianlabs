
package co.com.bcs.tin.xsd;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InitiateTransferSalType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="InitiateTransferSalType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="source" type="{http://xsd.tin.bcs.com.co}SourceDestinationType"/&gt;
 *         &lt;element name="destination" type="{http://xsd.tin.bcs.com.co}SourceDestinationType"/&gt;
 *         &lt;element name="amount" type="{http://xsd.tin.bcs.com.co}AmountType"/&gt;
 *         &lt;element name="transactions" type="{http://xsd.tin.bcs.com.co}TransactionsType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="metadata" type="{http://xsd.tin.bcs.com.co}MetadataType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InitiateTransferSalType",
         propOrder = { "source", "destination", "amount", "transactions", "metadata" })
public class InitiateTransferSalType {

    @XmlElement(required = true)
    protected SourceDestinationType source;
    @XmlElement(required = true)
    protected SourceDestinationType destination;
    @XmlElement(required = true)
    protected AmountType amount;
    protected List<TransactionsType> transactions;
    @XmlElement(required = true)
    protected MetadataType metadata;

    /**
     * Gets the value of the source property.
     *
     * @return
     *     possible object is
     *     {@link SourceDestinationType }
     *
     */
    public SourceDestinationType getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     *
     * @param value
     *     allowed object is
     *     {@link SourceDestinationType }
     *
     */
    public void setSource(SourceDestinationType value) {
        this.source = value;
    }

    /**
     * Gets the value of the destination property.
     *
     * @return
     *     possible object is
     *     {@link SourceDestinationType }
     *
     */
    public SourceDestinationType getDestination() {
        return destination;
    }

    /**
     * Sets the value of the destination property.
     *
     * @param value
     *     allowed object is
     *     {@link SourceDestinationType }
     *
     */
    public void setDestination(SourceDestinationType value) {
        this.destination = value;
    }

    /**
     * Gets the value of the amount property.
     *
     * @return
     *     possible object is
     *     {@link AmountType }
     *
     */
    public AmountType getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     *
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *
     */
    public void setAmount(AmountType value) {
        this.amount = value;
    }

    /**
     * Gets the value of the transactions property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transactions property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransactions().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransactionsType }
     *
     *
     */
    public List<TransactionsType> getTransactions() {
        if (transactions == null) {
            transactions = new ArrayList<TransactionsType>();
        }
        return this.transactions;
    }

    /**
     * Gets the value of the metadata property.
     *
     * @return
     *     possible object is
     *     {@link MetadataType }
     *
     */
    public MetadataType getMetadata() {
        return metadata;
    }

    /**
     * Sets the value of the metadata property.
     *
     * @param value
     *     allowed object is
     *     {@link MetadataType }
     *
     */
    public void setMetadata(MetadataType value) {
        this.metadata = value;
    }

}
