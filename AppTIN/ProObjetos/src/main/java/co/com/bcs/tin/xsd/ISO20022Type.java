
package co.com.bcs.tin.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ISO20022Type complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ISO20022Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="group_header" type="{http://xsd.tin.bcs.com.co}GroupHeaderType"/&gt;
 *         &lt;element name="credit_transfer_transaction_information" type="{http://xsd.tin.bcs.com.co}CreditTransferTXInformationType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ISO20022Type", propOrder = { "groupHeader", "creditTransferTransactionInformation" })
public class ISO20022Type {

    @XmlElement(name = "group_header", required = true)
    protected GroupHeaderType groupHeader;
    @XmlElement(name = "credit_transfer_transaction_information", required = true)
    protected CreditTransferTXInformationType creditTransferTransactionInformation;

    /**
     * Gets the value of the groupHeader property.
     *
     * @return
     *     possible object is
     *     {@link GroupHeaderType }
     *
     */
    public GroupHeaderType getGroupHeader() {
        return groupHeader;
    }

    /**
     * Sets the value of the groupHeader property.
     *
     * @param value
     *     allowed object is
     *     {@link GroupHeaderType }
     *
     */
    public void setGroupHeader(GroupHeaderType value) {
        this.groupHeader = value;
    }

    /**
     * Gets the value of the creditTransferTransactionInformation property.
     *
     * @return
     *     possible object is
     *     {@link CreditTransferTXInformationType }
     *
     */
    public CreditTransferTXInformationType getCreditTransferTransactionInformation() {
        return creditTransferTransactionInformation;
    }

    /**
     * Sets the value of the creditTransferTransactionInformation property.
     *
     * @param value
     *     allowed object is
     *     {@link CreditTransferTXInformationType }
     *
     */
    public void setCreditTransferTransactionInformation(CreditTransferTXInformationType value) {
        this.creditTransferTransactionInformation = value;
    }

}
