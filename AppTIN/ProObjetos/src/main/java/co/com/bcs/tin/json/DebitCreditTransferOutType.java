package co.com.bcs.tin.json;

import co.com.bcs.tin.xsd.AmountType;
import co.com.bcs.tin.xsd.MetadataType;
import co.com.bcs.tin.xsd.SourceDestinationType;
import co.com.bcs.tin.xsd.TransactionsType;

import java.util.List;

public class DebitCreditTransferOutType {

    protected SourceDestinationType source;
    protected SourceDestinationType destination;
    protected AmountType amount;
    protected List<TransactionsType> transactions;
    protected MetadataType metadata;


    public void setSource(SourceDestinationType source) {
        this.source = source;
    }

    public SourceDestinationType getSource() {
        return source;
    }

    public void setDestination(SourceDestinationType destination) {
        this.destination = destination;
    }

    public SourceDestinationType getDestination() {
        return destination;
    }

    public void setAmount(AmountType amount) {
        this.amount = amount;
    }

    public AmountType getAmount() {
        return amount;
    }

    public void setTransactions(List<TransactionsType> transactions) {
        this.transactions = transactions;
    }

    public List<TransactionsType> getTransactions() {
        return transactions;
    }

    public void setMetadata(MetadataType metadata) {
        this.metadata = metadata;
    }

    public MetadataType getMetadata() {
        return metadata;
    }
}
