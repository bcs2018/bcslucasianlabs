
package co.com.bcs.tin.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreditTransferTXInformationType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="CreditTransferTXInformationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="debtor" type="{http://xsd.tin.bcs.com.co}InfoClientType"/&gt;
 *         &lt;element name="debtorAgent" type="{http://xsd.tin.bcs.com.co}DebtorAgentType"/&gt;
 *         &lt;element name="debtorAccount" type="{http://xsd.tin.bcs.com.co}DebtorAccountType"/&gt;
 *         &lt;element name="creditor" type="{http://xsd.tin.bcs.com.co}InfoClientType"/&gt;
 *         &lt;element name="creditorAccount" type="{http://xsd.tin.bcs.com.co}CreditorAccountType"/&gt;
 *         &lt;element name="creditorAgent" type="{http://xsd.tin.bcs.com.co}CreditorAgentType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreditTransferTXInformationType",
         propOrder = { "debtor", "debtorAgent", "debtorAccount", "creditor", "creditorAccount", "creditorAgent"
    })
public class CreditTransferTXInformationType {

    @XmlElement(required = true)
    protected InfoClientType debtor;
    @XmlElement(required = true)
    protected DebtorAgentType debtorAgent;
    @XmlElement(required = true)
    protected DebtorAccountType debtorAccount;
    @XmlElement(required = true)
    protected InfoClientType creditor;
    @XmlElement(required = true)
    protected CreditorAccountType creditorAccount;
    @XmlElement(required = true)
    protected CreditorAgentType creditorAgent;

    /**
     * Gets the value of the debtor property.
     *
     * @return
     *     possible object is
     *     {@link InfoClientType }
     *
     */
    public InfoClientType getDebtor() {
        return debtor;
    }

    /**
     * Sets the value of the debtor property.
     *
     * @param value
     *     allowed object is
     *     {@link InfoClientType }
     *
     */
    public void setDebtor(InfoClientType value) {
        this.debtor = value;
    }

    /**
     * Gets the value of the debtorAgent property.
     *
     * @return
     *     possible object is
     *     {@link DebtorAgentType }
     *
     */
    public DebtorAgentType getDebtorAgent() {
        return debtorAgent;
    }

    /**
     * Sets the value of the debtorAgent property.
     *
     * @param value
     *     allowed object is
     *     {@link DebtorAgentType }
     *
     */
    public void setDebtorAgent(DebtorAgentType value) {
        this.debtorAgent = value;
    }

    /**
     * Gets the value of the debtorAccount property.
     *
     * @return
     *     possible object is
     *     {@link DebtorAccountType }
     *
     */
    public DebtorAccountType getDebtorAccount() {
        return debtorAccount;
    }

    /**
     * Sets the value of the debtorAccount property.
     *
     * @param value
     *     allowed object is
     *     {@link DebtorAccountType }
     *
     */
    public void setDebtorAccount(DebtorAccountType value) {
        this.debtorAccount = value;
    }

    /**
     * Gets the value of the creditor property.
     *
     * @return
     *     possible object is
     *     {@link InfoClientType }
     *
     */
    public InfoClientType getCreditor() {
        return creditor;
    }

    /**
     * Sets the value of the creditor property.
     *
     * @param value
     *     allowed object is
     *     {@link InfoClientType }
     *
     */
    public void setCreditor(InfoClientType value) {
        this.creditor = value;
    }

    /**
     * Gets the value of the creditorAccount property.
     *
     * @return
     *     possible object is
     *     {@link CreditorAccountType }
     *
     */
    public CreditorAccountType getCreditorAccount() {
        return creditorAccount;
    }

    /**
     * Sets the value of the creditorAccount property.
     *
     * @param value
     *     allowed object is
     *     {@link CreditorAccountType }
     *
     */
    public void setCreditorAccount(CreditorAccountType value) {
        this.creditorAccount = value;
    }

    /**
     * Gets the value of the creditorAgent property.
     *
     * @return
     *     possible object is
     *     {@link CreditorAgentType }
     *
     */
    public CreditorAgentType getCreditorAgent() {
        return creditorAgent;
    }

    /**
     * Sets the value of the creditorAgent property.
     *
     * @param value
     *     allowed object is
     *     {@link CreditorAgentType }
     *
     */
    public void setCreditorAgent(CreditorAgentType value) {
        this.creditorAgent = value;
    }

}
