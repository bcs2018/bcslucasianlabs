
package co.com.bcs.tin.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ClearingSystemType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="ClearingSystemType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="proprietary" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ClearingSystemType", propOrder = { "proprietary" })
public class ClearingSystemType {

    @XmlElement(required = true)
    protected String proprietary;

    /**
     * Gets the value of the proprietary property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getProprietary() {
        return proprietary;
    }

    /**
     * Sets the value of the proprietary property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setProprietary(String value) {
        this.proprietary = value;
    }

}
