
package co.com.bcs.tin.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InstructingAgentType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="InstructingAgentType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="finantial_institution_identification" type="{http://xsd.tin.bcs.com.co}FinInstitutionIdentificationType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InstructingAgentType", propOrder = { "finantialInstitutionIdentification" })
public class InstructingAgentType {

    @XmlElement(name = "finantial_institution_identification", required = true)
    protected FinInstitutionIdentificationType finantialInstitutionIdentification;

    /**
     * Gets the value of the finantialInstitutionIdentification property.
     *
     * @return
     *     possible object is
     *     {@link FinInstitutionIdentificationType }
     *
     */
    public FinInstitutionIdentificationType getFinantialInstitutionIdentification() {
        return finantialInstitutionIdentification;
    }

    /**
     * Sets the value of the finantialInstitutionIdentification property.
     *
     * @param value
     *     allowed object is
     *     {@link FinInstitutionIdentificationType }
     *
     */
    public void setFinantialInstitutionIdentification(FinInstitutionIdentificationType value) {
        this.finantialInstitutionIdentification = value;
    }

}
