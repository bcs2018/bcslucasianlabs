
package co.com.bcs.tin.xsd;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MetadataType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="MetadataType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="txNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="transactions_purpose" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="channel" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="person_type_origen" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="preserve_debtor" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="clearingSystem" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="txid" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="created" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="personTypeDestination" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="preserveCreditor" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="stateTrustLink" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="iso_20022_pac_008_001_07" type="{http://xsd.tin.bcs.com.co}ISO20022Type"/&gt;
 *         &lt;element name="iso_20022_pac_002_001_009" type="{http://xsd.tin.bcs.com.co}ISO20022Type"/&gt;
 *         &lt;element name="anti_fraud_analysis" type="{http://xsd.tin.bcs.com.co}AntiFraudAnalysisType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="trust_link" type="{http://xsd.tin.bcs.com.co}TrustLinkType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadataType",
         propOrder =
         { "type", "status", "txNumber", "description", "transactionsPurpose", "channel", "personTypeOrigen",
           "preserveDebtor", "clearingSystem", "txid", "created", "personTypeDestination", "preserveCreditor",
           "stateTrustLink", "iso20022Pac00800107", "iso20022Pac002001009", "antiFraudAnalysis", "trustLink"
    })
public class MetadataType {

    @XmlElement(required = true)
    protected String type;
    @XmlElement(required = true)
    protected String status;
    @XmlElement(required = true)
    protected String txNumber;
    @XmlElement(required = true)
    protected String description;
    @XmlElement(name = "transactions_purpose", required = true)
    protected String transactionsPurpose;
    @XmlElement(required = true)
    protected String channel;
    @XmlElement(name = "person_type_origen", required = true)
    protected String personTypeOrigen;
    @XmlElement(name = "preserve_debtor", required = true)
    protected String preserveDebtor;
    @XmlElement(required = true)
    protected String clearingSystem;
    @XmlElement(required = true)
    protected String txid;
    @XmlElement(required = true)
    protected String created;
    @XmlElement(required = true)
    protected String personTypeDestination;
    @XmlElement(required = true)
    protected String preserveCreditor;
    @XmlElement(required = true)
    protected String stateTrustLink;
    @XmlElement(name = "iso_20022_pac_008_001_07", required = true)
    protected ISO20022Type iso20022Pac00800107;
    @XmlElement(name = "iso_20022_pac_002_001_009", required = true)
    protected ISO20022Type iso20022Pac002001009;
    @XmlElement(name = "anti_fraud_analysis")
    protected List<AntiFraudAnalysisType> antiFraudAnalysis;
    @XmlElement(name = "trust_link", required = true)
    protected TrustLinkType trustLink;

    /**
     * Gets the value of the type property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * Gets the value of the status property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the txNumber property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTxNumber() {
        return txNumber;
    }

    /**
     * Sets the value of the txNumber property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTxNumber(String value) {
        this.txNumber = value;
    }

    /**
     * Gets the value of the description property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the transactionsPurpose property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTransactionsPurpose() {
        return transactionsPurpose;
    }

    /**
     * Sets the value of the transactionsPurpose property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTransactionsPurpose(String value) {
        this.transactionsPurpose = value;
    }

    /**
     * Gets the value of the channel property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getChannel() {
        return channel;
    }

    /**
     * Sets the value of the channel property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setChannel(String value) {
        this.channel = value;
    }

    /**
     * Gets the value of the personTypeOrigen property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPersonTypeOrigen() {
        return personTypeOrigen;
    }

    /**
     * Sets the value of the personTypeOrigen property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPersonTypeOrigen(String value) {
        this.personTypeOrigen = value;
    }

    /**
     * Gets the value of the preserveDebtor property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPreserveDebtor() {
        return preserveDebtor;
    }

    /**
     * Sets the value of the preserveDebtor property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPreserveDebtor(String value) {
        this.preserveDebtor = value;
    }

    /**
     * Gets the value of the clearingSystem property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getClearingSystem() {
        return clearingSystem;
    }

    /**
     * Sets the value of the clearingSystem property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setClearingSystem(String value) {
        this.clearingSystem = value;
    }

    /**
     * Gets the value of the txid property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTxid() {
        return txid;
    }

    /**
     * Sets the value of the txid property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTxid(String value) {
        this.txid = value;
    }

    /**
     * Gets the value of the created property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCreated() {
        return created;
    }

    /**
     * Sets the value of the created property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCreated(String value) {
        this.created = value;
    }

    /**
     * Gets the value of the personTypeDestination property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPersonTypeDestination() {
        return personTypeDestination;
    }

    /**
     * Sets the value of the personTypeDestination property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPersonTypeDestination(String value) {
        this.personTypeDestination = value;
    }

    /**
     * Gets the value of the preserveCreditor property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPreserveCreditor() {
        return preserveCreditor;
    }

    /**
     * Sets the value of the preserveCreditor property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPreserveCreditor(String value) {
        this.preserveCreditor = value;
    }

    /**
     * Gets the value of the stateTrustLink property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStateTrustLink() {
        return stateTrustLink;
    }

    /**
     * Sets the value of the stateTrustLink property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStateTrustLink(String value) {
        this.stateTrustLink = value;
    }

    /**
     * Gets the value of the iso20022Pac00800107 property.
     *
     * @return
     *     possible object is
     *     {@link ISO20022Type }
     *
     */
    public ISO20022Type getIso20022Pac00800107() {
        return iso20022Pac00800107;
    }

    /**
     * Sets the value of the iso20022Pac00800107 property.
     *
     * @param value
     *     allowed object is
     *     {@link ISO20022Type }
     *
     */
    public void setIso20022Pac00800107(ISO20022Type value) {
        this.iso20022Pac00800107 = value;
    }

    /**
     * Gets the value of the iso20022Pac002001009 property.
     *
     * @return
     *     possible object is
     *     {@link ISO20022Type }
     *
     */
    public ISO20022Type getIso20022Pac002001009() {
        return iso20022Pac002001009;
    }

    /**
     * Sets the value of the iso20022Pac002001009 property.
     *
     * @param value
     *     allowed object is
     *     {@link ISO20022Type }
     *
     */
    public void setIso20022Pac002001009(ISO20022Type value) {
        this.iso20022Pac002001009 = value;
    }

    /**
     * Gets the value of the antiFraudAnalysis property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the antiFraudAnalysis property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAntiFraudAnalysis().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AntiFraudAnalysisType }
     *
     *
     */
    public List<AntiFraudAnalysisType> getAntiFraudAnalysis() {
        if (antiFraudAnalysis == null) {
            antiFraudAnalysis = new ArrayList<AntiFraudAnalysisType>();
        }
        return this.antiFraudAnalysis;
    }

    /**
     * Gets the value of the trustLink property.
     *
     * @return
     *     possible object is
     *     {@link TrustLinkType }
     *
     */
    public TrustLinkType getTrustLink() {
        return trustLink;
    }

    /**
     * Sets the value of the trustLink property.
     *
     * @param value
     *     allowed object is
     *     {@link TrustLinkType }
     *
     */
    public void setTrustLink(TrustLinkType value) {
        this.trustLink = value;
    }

}
