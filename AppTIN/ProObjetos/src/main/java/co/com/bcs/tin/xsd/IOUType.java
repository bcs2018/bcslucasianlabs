
package co.com.bcs.tin.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IOUType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="IOUType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="IOU_OBJECT" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IOUType", propOrder = {


    })
public class IOUType {

    @XmlElement(name = "IOU_OBJECT", required = true)
    protected Object IOU_OBJECT;

    /**
     * Gets the value of the iouobject property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public Object getIOUOBJECT() {
        return IOU_OBJECT;
    }

    /**
     * Sets the value of the iouobject property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setIOUOBJECT(Object value) {
        this.IOU_OBJECT = value;
    }

}
