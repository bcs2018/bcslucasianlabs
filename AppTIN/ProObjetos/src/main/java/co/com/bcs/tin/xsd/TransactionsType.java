
package co.com.bcs.tin.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TransactionsType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="TransactionsType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="alias" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="tx_idx" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="iou" type="{http://xsd.tin.bcs.com.co}IOUType"/&gt;
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionsType", propOrder = { "alias", "txIdx", "iou", "type" })
public class TransactionsType {

    @XmlElement(required = true)
    protected String alias;
    @XmlElement(name = "tx_idx", required = true)
    protected String txIdx;
    @XmlElement(required = true)
    protected IOUType iou;
    @XmlElement(required = true)
    protected String type;

    /**
     * Gets the value of the alias property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAlias() {
        return alias;
    }

    /**
     * Sets the value of the alias property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAlias(String value) {
        this.alias = value;
    }

    /**
     * Gets the value of the txIdx property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTxIdx() {
        return txIdx;
    }

    /**
     * Sets the value of the txIdx property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTxIdx(String value) {
        this.txIdx = value;
    }

    /**
     * Gets the value of the iou property.
     *
     * @return
     *     possible object is
     *     {@link IOUType }
     *
     */
    public IOUType getIou() {
        return iou;
    }

    /**
     * Sets the value of the iou property.
     *
     * @param value
     *     allowed object is
     *     {@link IOUType }
     *
     */
    public void setIou(IOUType value) {
        this.iou = value;
    }

    /**
     * Gets the value of the type property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setType(String value) {
        this.type = value;
    }

}
