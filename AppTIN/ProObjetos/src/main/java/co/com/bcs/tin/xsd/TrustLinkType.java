
package co.com.bcs.tin.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TrustLinkType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="TrustLinkType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="source" type="{http://xsd.tin.bcs.com.co}userType"/&gt;
 *         &lt;element name="destination" type="{http://xsd.tin.bcs.com.co}userType"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TrustLinkType", propOrder = {


    })
public class TrustLinkType {

    @XmlElement(required = true)
    protected UserType source;
    @XmlElement(required = true)
    protected UserType destination;

    /**
     * Gets the value of the source property.
     *
     * @return
     *     possible object is
     *     {@link UserType }
     *
     */
    public UserType getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     *
     * @param value
     *     allowed object is
     *     {@link UserType }
     *
     */
    public void setSource(UserType value) {
        this.source = value;
    }

    /**
     * Gets the value of the destination property.
     *
     * @return
     *     possible object is
     *     {@link UserType }
     *
     */
    public UserType getDestination() {
        return destination;
    }

    /**
     * Sets the value of the destination property.
     *
     * @param value
     *     allowed object is
     *     {@link UserType }
     *
     */
    public void setDestination(UserType value) {
        this.destination = value;
    }

}
