
package co.com.bcs.tin.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for identificationType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="identificationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="privateIdentification" type="{http://xsd.tin.bcs.com.co}PrivateIdentificationType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "identificationType", propOrder = { "privateIdentification" })
public class IdentificationType {

    @XmlElement(required = true)
    protected PrivateIdentificationType privateIdentification;

    /**
     * Gets the value of the privateIdentification property.
     *
     * @return
     *     possible object is
     *     {@link PrivateIdentificationType }
     *
     */
    public PrivateIdentificationType getPrivateIdentification() {
        return privateIdentification;
    }

    /**
     * Sets the value of the privateIdentification property.
     *
     * @param value
     *     allowed object is
     *     {@link PrivateIdentificationType }
     *
     */
    public void setPrivateIdentification(PrivateIdentificationType value) {
        this.privateIdentification = value;
    }

}
