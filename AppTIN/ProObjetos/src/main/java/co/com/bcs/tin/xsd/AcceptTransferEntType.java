
package co.com.bcs.tin.xsd;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AcceptTransferEntType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="AcceptTransferEntType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="source" type="{http://xsd.tin.bcs.com.co}SourceDestinationType"/&gt;
 *         &lt;element name="destination" type="{http://xsd.tin.bcs.com.co}SourceDestinationType"/&gt;
 *         &lt;element name="sourceOrDestinationPhone" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="sourceBankCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="destinationBankCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="sourceOrDestinationBankCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AcceptTransferEntType", propOrder = {


    })
public class AcceptTransferEntType {

    @XmlElement(required = true)
    protected SourceDestinationType source;
    @XmlElement(required = true)
    protected SourceDestinationType destination;
    @XmlElement(required = true)
    protected BigDecimal sourceOrDestinationPhone;
    @XmlElement(required = true)
    protected String sourceBankCode;
    @XmlElement(required = true)
    protected String destinationBankCode;
    @XmlElement(required = true)
    protected String sourceOrDestinationBankCode;
    @XmlElement(required = true)
    protected String status;

    /**
     * Gets the value of the source property.
     *
     * @return
     *     possible object is
     *     {@link SourceDestinationType }
     *
     */
    public SourceDestinationType getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     *
     * @param value
     *     allowed object is
     *     {@link SourceDestinationType }
     *
     */
    public void setSource(SourceDestinationType value) {
        this.source = value;
    }

    /**
     * Gets the value of the destination property.
     *
     * @return
     *     possible object is
     *     {@link SourceDestinationType }
     *
     */
    public SourceDestinationType getDestination() {
        return destination;
    }

    /**
     * Sets the value of the destination property.
     *
     * @param value
     *     allowed object is
     *     {@link SourceDestinationType }
     *
     */
    public void setDestination(SourceDestinationType value) {
        this.destination = value;
    }

    /**
     * Gets the value of the sourceOrDestinationPhone property.
     *
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *
     */
    public BigDecimal getSourceOrDestinationPhone() {
        return sourceOrDestinationPhone;
    }

    /**
     * Sets the value of the sourceOrDestinationPhone property.
     *
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *
     */
    public void setSourceOrDestinationPhone(BigDecimal value) {
        this.sourceOrDestinationPhone = value;
    }

    /**
     * Gets the value of the sourceBankCode property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSourceBankCode() {
        return sourceBankCode;
    }

    /**
     * Sets the value of the sourceBankCode property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSourceBankCode(String value) {
        this.sourceBankCode = value;
    }

    /**
     * Gets the value of the destinationBankCode property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDestinationBankCode() {
        return destinationBankCode;
    }

    /**
     * Sets the value of the destinationBankCode property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDestinationBankCode(String value) {
        this.destinationBankCode = value;
    }

    /**
     * Gets the value of the sourceOrDestinationBankCode property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSourceOrDestinationBankCode() {
        return sourceOrDestinationBankCode;
    }

    /**
     * Sets the value of the sourceOrDestinationBankCode property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSourceOrDestinationBankCode(String value) {
        this.sourceOrDestinationBankCode = value;
    }

    /**
     * Gets the value of the status property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setStatus(String value) {
        this.status = value;
    }

}
