
package co.com.bcs.tin.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AntiFraudAnalysisType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="AntiFraudAnalysisType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="processor" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="fraud" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="score" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AntiFraudAnalysisType", propOrder = { "processor", "fraud", "score" })
public class AntiFraudAnalysisType {

    @XmlElement(required = true)
    protected String processor;
    @XmlElement(required = true)
    protected String fraud;
    @XmlElement(required = true)
    protected String score;

    /**
     * Gets the value of the processor property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getProcessor() {
        return processor;
    }

    /**
     * Sets the value of the processor property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setProcessor(String value) {
        this.processor = value;
    }

    /**
     * Gets the value of the fraud property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getFraud() {
        return fraud;
    }

    /**
     * Sets the value of the fraud property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setFraud(String value) {
        this.fraud = value;
    }

    /**
     * Gets the value of the score property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getScore() {
        return score;
    }

    /**
     * Sets the value of the score property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setScore(String value) {
        this.score = value;
    }

}
