
package co.com.bcs.tin.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OriginalGroupInfoAndStatusType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="OriginalGroupInfoAndStatusType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="originalMessageID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="originalMessageNameID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="groupStatus" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="statusReasonInformation" type="{http://xsd.tin.bcs.com.co}StatusReasonInformationType"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OriginalGroupInfoAndStatusType",
         propOrder = { "originalMessageID", "originalMessageNameID", "groupStatus", "statusReasonInformation"
    })
public class OriginalGroupInfoAndStatusType {

    @XmlElement(required = true)
    protected String originalMessageID;
    @XmlElement(required = true)
    protected String originalMessageNameID;
    @XmlElement(required = true)
    protected String groupStatus;
    @XmlElement(required = true)
    protected StatusReasonInformationType statusReasonInformation;

    /**
     * Gets the value of the originalMessageID property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOriginalMessageID() {
        return originalMessageID;
    }

    /**
     * Sets the value of the originalMessageID property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOriginalMessageID(String value) {
        this.originalMessageID = value;
    }

    /**
     * Gets the value of the originalMessageNameID property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getOriginalMessageNameID() {
        return originalMessageNameID;
    }

    /**
     * Sets the value of the originalMessageNameID property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setOriginalMessageNameID(String value) {
        this.originalMessageNameID = value;
    }

    /**
     * Gets the value of the groupStatus property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getGroupStatus() {
        return groupStatus;
    }

    /**
     * Sets the value of the groupStatus property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setGroupStatus(String value) {
        this.groupStatus = value;
    }

    /**
     * Gets the value of the statusReasonInformation property.
     *
     * @return
     *     possible object is
     *     {@link StatusReasonInformationType }
     *
     */
    public StatusReasonInformationType getStatusReasonInformation() {
        return statusReasonInformation;
    }

    /**
     * Sets the value of the statusReasonInformation property.
     *
     * @param value
     *     allowed object is
     *     {@link StatusReasonInformationType }
     *
     */
    public void setStatusReasonInformation(StatusReasonInformationType value) {
        this.statusReasonInformation = value;
    }

}
