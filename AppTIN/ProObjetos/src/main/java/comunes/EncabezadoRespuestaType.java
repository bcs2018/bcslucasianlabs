
package comunes;

import java.math.BigInteger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EncabezadoRespuesta_Type complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="EncabezadoRespuesta_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="codigoRespuesta" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="severidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="descripcionEstado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="idTransaccionOrigen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="fechaRespuesta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="idioma" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EncabezadoRespuesta_Type",
         propOrder =
         { "codigoRespuesta", "severidad", "descripcionEstado", "idTransaccionOrigen", "fechaRespuesta", "idioma"
    })
public class EncabezadoRespuestaType {

    protected BigInteger codigoRespuesta;
    protected String severidad;
    protected String descripcionEstado;
    protected String idTransaccionOrigen;
    protected String fechaRespuesta;
    protected String idioma;

    /**
     * Gets the value of the codigoRespuesta property.
     *
     * @return
     *     possible object is
     *     {@link BigInteger }
     *
     */
    public BigInteger getCodigoRespuesta() {
        return codigoRespuesta;
    }

    /**
     * Sets the value of the codigoRespuesta property.
     *
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *
     */
    public void setCodigoRespuesta(BigInteger value) {
        this.codigoRespuesta = value;
    }

    /**
     * Gets the value of the severidad property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSeveridad() {
        return severidad;
    }

    /**
     * Sets the value of the severidad property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSeveridad(String value) {
        this.severidad = value;
    }

    /**
     * Gets the value of the descripcionEstado property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getDescripcionEstado() {
        return descripcionEstado;
    }

    /**
     * Sets the value of the descripcionEstado property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setDescripcionEstado(String value) {
        this.descripcionEstado = value;
    }

    /**
     * Gets the value of the idTransaccionOrigen property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getIdTransaccionOrigen() {
        return idTransaccionOrigen;
    }

    /**
     * Sets the value of the idTransaccionOrigen property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setIdTransaccionOrigen(String value) {
        this.idTransaccionOrigen = value;
    }

    /**
     * Gets the value of the fechaRespuesta property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getFechaRespuesta() {
        return fechaRespuesta;
    }

    /**
     * Sets the value of the fechaRespuesta property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setFechaRespuesta(String value) {
        this.fechaRespuesta = value;
    }

    /**
     * Gets the value of the idioma property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getIdioma() {
        return idioma;
    }

    /**
     * Sets the value of the idioma property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setIdioma(String value) {
        this.idioma = value;
    }

}
