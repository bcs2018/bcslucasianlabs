
package comunes;

import java.math.BigInteger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Encabezado_Type complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="Encabezado_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="idTransaccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="codigoCanal" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="codigoOficina" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="codigoCajero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="tipoTerminal" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="codigoBanco" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="entorno" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="tipoCajero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="idCajero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="sucursal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="fechaContable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="fechaVencimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="horario" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Encabezado_Type",
         propOrder =
         { "idTransaccion", "codigoCanal", "codigoOficina", "codigoCajero", "tipoTerminal", "codigoBanco", "entorno",
           "tipoCajero", "idCajero", "sucursal", "fechaContable", "fechaVencimiento", "horario"
    })
public class EncabezadoType {

    protected String idTransaccion;
    protected BigInteger codigoCanal;
    protected String codigoOficina;
    protected String codigoCajero;
    protected BigInteger tipoTerminal;
    protected BigInteger codigoBanco;
    protected BigInteger entorno;
    protected String tipoCajero;
    protected String idCajero;
    protected String sucursal;
    protected String fechaContable;
    protected String fechaVencimiento;
    protected BigInteger horario;

    /**
     * Gets the value of the idTransaccion property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getIdTransaccion() {
        return idTransaccion;
    }

    /**
     * Sets the value of the idTransaccion property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setIdTransaccion(String value) {
        this.idTransaccion = value;
    }

    /**
     * Gets the value of the codigoCanal property.
     *
     * @return
     *     possible object is
     *     {@link BigInteger }
     *
     */
    public BigInteger getCodigoCanal() {
        return codigoCanal;
    }

    /**
     * Sets the value of the codigoCanal property.
     *
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *
     */
    public void setCodigoCanal(BigInteger value) {
        this.codigoCanal = value;
    }

    /**
     * Gets the value of the codigoOficina property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCodigoOficina() {
        return codigoOficina;
    }

    /**
     * Sets the value of the codigoOficina property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCodigoOficina(String value) {
        this.codigoOficina = value;
    }

    /**
     * Gets the value of the codigoCajero property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getCodigoCajero() {
        return codigoCajero;
    }

    /**
     * Sets the value of the codigoCajero property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setCodigoCajero(String value) {
        this.codigoCajero = value;
    }

    /**
     * Gets the value of the tipoTerminal property.
     *
     * @return
     *     possible object is
     *     {@link BigInteger }
     *
     */
    public BigInteger getTipoTerminal() {
        return tipoTerminal;
    }

    /**
     * Sets the value of the tipoTerminal property.
     *
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *
     */
    public void setTipoTerminal(BigInteger value) {
        this.tipoTerminal = value;
    }

    /**
     * Gets the value of the codigoBanco property.
     *
     * @return
     *     possible object is
     *     {@link BigInteger }
     *
     */
    public BigInteger getCodigoBanco() {
        return codigoBanco;
    }

    /**
     * Sets the value of the codigoBanco property.
     *
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *
     */
    public void setCodigoBanco(BigInteger value) {
        this.codigoBanco = value;
    }

    /**
     * Gets the value of the entorno property.
     *
     * @return
     *     possible object is
     *     {@link BigInteger }
     *
     */
    public BigInteger getEntorno() {
        return entorno;
    }

    /**
     * Sets the value of the entorno property.
     *
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *
     */
    public void setEntorno(BigInteger value) {
        this.entorno = value;
    }

    /**
     * Gets the value of the tipoCajero property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTipoCajero() {
        return tipoCajero;
    }

    /**
     * Sets the value of the tipoCajero property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTipoCajero(String value) {
        this.tipoCajero = value;
    }

    /**
     * Gets the value of the idCajero property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getIdCajero() {
        return idCajero;
    }

    /**
     * Sets the value of the idCajero property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setIdCajero(String value) {
        this.idCajero = value;
    }

    /**
     * Gets the value of the sucursal property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getSucursal() {
        return sucursal;
    }

    /**
     * Sets the value of the sucursal property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setSucursal(String value) {
        this.sucursal = value;
    }

    /**
     * Gets the value of the fechaContable property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getFechaContable() {
        return fechaContable;
    }

    /**
     * Sets the value of the fechaContable property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setFechaContable(String value) {
        this.fechaContable = value;
    }

    /**
     * Gets the value of the fechaVencimiento property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    /**
     * Sets the value of the fechaVencimiento property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setFechaVencimiento(String value) {
        this.fechaVencimiento = value;
    }

    /**
     * Gets the value of the horario property.
     *
     * @return
     *     possible object is
     *     {@link BigInteger }
     *
     */
    public BigInteger getHorario() {
        return horario;
    }

    /**
     * Sets the value of the horario property.
     *
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *
     */
    public void setHorario(BigInteger value) {
        this.horario = value;
    }

}
