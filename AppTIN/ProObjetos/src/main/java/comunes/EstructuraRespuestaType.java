
package comunes;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EstructuraRespuesta_Type complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="EstructuraRespuesta_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="TipoRespuesta"&gt;
 *           &lt;simpleType&gt;
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *               &lt;length value="2"/&gt;
 *               &lt;enumeration value="OK"/&gt;
 *               &lt;enumeration value="ER"/&gt;
 *             &lt;/restriction&gt;
 *           &lt;/simpleType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="RespuestaError" type="{comunes}SalRespuestaER_Type" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EstructuraRespuesta_Type", propOrder = { "tipoRespuesta", "respuestaError" })
public class EstructuraRespuestaType {

    @XmlElement(name = "TipoRespuesta", required = true)
    protected String tipoRespuesta;
    @XmlElement(name = "RespuestaError")
    protected SalRespuestaERType respuestaError;

    /**
     * Gets the value of the tipoRespuesta property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTipoRespuesta() {
        return tipoRespuesta;
    }

    /**
     * Sets the value of the tipoRespuesta property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTipoRespuesta(String value) {
        this.tipoRespuesta = value;
    }

    /**
     * Gets the value of the respuestaError property.
     *
     * @return
     *     possible object is
     *     {@link SalRespuestaERType }
     *
     */
    public SalRespuestaERType getRespuestaError() {
        return respuestaError;
    }

    /**
     * Sets the value of the respuestaError property.
     *
     * @param value
     *     allowed object is
     *     {@link SalRespuestaERType }
     *
     */
    public void setRespuestaError(SalRespuestaERType value) {
        this.respuestaError = value;
    }

}
