
package auth;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for responseType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="responseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="clientId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="accessToken" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="idToken" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="refreshToken" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tokenType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="expireIn" type="{http://www.w3.org/2001/XMLSchema}Integer"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "responseType", propOrder = { })
public class ResponseType {

    @XmlElement(required = true)
    protected String clientId;
    @XmlElement(required = true)
    protected String accessToken;
    @XmlElement(required = true)
    protected String idToken;
    @XmlElement(required = true)
    protected String refreshToken;
    @XmlElement(required = true)
    protected String tokenType;
    @XmlElement(required = true)
    protected Integer expireIn;

    /**
     * Gets the value of the clientId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * Sets the value of the clientId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setClientId(String value) {
        this.clientId = value;
    }

    /**
     * Gets the value of the accessToken property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAccessToken() {
        return accessToken;
    }

    /**
     * Sets the value of the accessToken property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAccessToken(String value) {
        this.accessToken = value;
    }
    /**
     * Gets the value of the idToken property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getIdToken() {
        return idToken;
    }

    /**
     * Sets the value of the idToken property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setIdToken(String value) {
        this.idToken = value;
    }
    /**
     * Gets the value of the refreshToken property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getRefreshToken() {
        return refreshToken;
    }

    /**
     * Sets the value of the refreshToken property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setRefreshToken(String value) {
        this.refreshToken = value;
    }
    /**
     * Gets the value of the tokenType property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getTokenType() {
        return tokenType;
    }

    /**
     * Sets the value of the tokenType property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTokenType(String value) {
        this.tokenType = value;
    }
    /**
     * Gets the value of the expireIn property.
     *
     * @return
     *     possible object is
     *     {@link Integer }
     *
     */
    public Integer getExpireIn() {
        return expireIn;
    }

    /**
     * Sets the value of the expireIn property.
     *
     * @param value
     *     allowed object is
     *     {@link Integer }
     *
     */
    public void setExpireIn(Integer value) {
        this.expireIn = value;
    }


}
