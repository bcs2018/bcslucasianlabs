
package auth;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for requestType complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType name="requestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="grantType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="clientId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="clientSecret" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nit" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tipoDocumento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nroDocumento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="authorization" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "requestType", propOrder = { })
public class RequestType {
    
    @XmlElement(required = true)
    protected String clientId;
    //tokens/login
    @XmlElement(required = true)
    protected String grantType;
    @XmlElement(required = true)
    protected String clientSecret;
    @XmlElement(required = true)
    protected String nit;
    @XmlElement(required = true)
    protected String tipoDocumento;
    @XmlElement(required = true)
    protected String nroDocumento;
    @XmlElement(required = true)
    protected String password;
      
    //tokens/refresh
    //tokens/revoke
    @XmlElement(required = true)
    protected String authorization;

    /**
     * Gets the value of the grantType property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getGrantType() {
        return grantType;
    }

    /**
     * Sets the value of the grantType property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setGrantType(String value) {
        this.grantType = value;
    }

    /**
     * Gets the value of the clientId property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * Sets the value of the clientId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setClientId(String value) {
        this.clientId = value;
    }

    /**
     * Gets the value of the clientSecret property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getClientSecret() {
        return clientSecret;
    }

    /**
     * Sets the value of the clientSecret property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setClientSecret(String value) {
        this.clientSecret = value;
    }

    /**
     * Gets the value of the nit property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getNit() {
        return clientId;
    }

    /**
     * Sets the value of the clientId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setNit(String value) {
        this.nit = value;
    }

    /**
     * Gets the value of the tipoDocumento property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getnroDocumento() {
        return nroDocumento;
    }

    /**
     * Sets the value of the tipoDocumento property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setTipoDocumento(String value) {
        this.tipoDocumento = value;
    }

    /**
     * Gets the value of the nroDocumento property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getNroDocumento() {
        return nroDocumento;
    }

    /**
     * Sets the value of the clientId property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setNroDocumento(String value) {
        this.nroDocumento = value;
    }

    /**
     * Gets the value of the password property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setPassword(String value) {
        this.password = value;
    }





    
    /**
     * Gets the value of the authorization property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getAuthorization() {
        return authorization;
    }

    /**
     * Sets the value of the authorization property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setAuthorization(String value) {
        this.authorization = value;
    }

  
}
