package types.json;

public class Identidad {
    String clientId;
    String tocken;
    String refreshTocken;
    String fechaHora;
    String expireIn;

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setTocken(String tocken) {
        this.tocken = tocken;
    }

    public String getTocken() {
        return tocken;
    }

    public void setRefreshTocken(String refreshTocken) {
        this.refreshTocken = refreshTocken;
    }

    public String getRefreshTocken() {
        return refreshTocken;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    public String getFechaHora() {
        return fechaHora;
    }

    public void setExpireIn(String expireIn) {
        this.expireIn = expireIn;
    }

    public String getExpireIn() {
        return expireIn;
    }
}
